# First, the Theory Behind this seed
In todays world, the majority of web applications have extremely similar features.  This seed is an attempt to create an extremely robust boilerplate for these kinds of applications.  Specifically, it is for applications that help service based businesses by automating processes, increasing user engagement, and automating bookkeeping.  If you make enough of these, you realize that just about every single application has a lot of the same features:
- authentication for users with login / signup / forgot password / facebook and gmail login / etc
- permissions so that only certain users can view certain pages
- element layering scss to make things look fancy
- various modules for design such as material design and flex layout
- payment through stripe or paypal
- scheduling / calendar feature
- pages such as faq, service explanations, team show off, about, etc
- dynamic header based on screen size
- dynamically sized text and a standardized color theme
- SEO optimization and specific meta tags for sharing on social media
- automatic error handling and tracking
- robust state management system
- shared animations for fading in and out, moving elements on/off the screen, etc
- contact form
- analytics and metrics
- email lists / push notifications
- progressive web app capability
- chat / messaging
- social media / commenting / etc


If you talk to a company, start up, or service based business, they want all of these features.  It doesn't matter what you are making, these features are almost universally needed for user driven applications for service based businesses.  Authentication creates users which the app is meant for, scheduling allows people to easily use whatever service you sell, automatic payment lets them buy that service, permissions can gamify an application or give special rights to paying members, SEO brings in more people, error handling keeps your application from falling apart, push notifications engage your users in ways that email can't.  The only difference between these features in different applications are how they interact with the customer on the front end.  That changes dynamically based on the needs and desires of the business and their customer base.

The seeds goal is to allow you to build out a complex idea for a large, monetized application in less than one month.  By 'large', I mean an application that would take 6-12 months to build from scratch.

# Overview
This is a starter for enterprise level, user driven progressive web applications.  features inclulde:
- full state management with NGRX using their route-store and entities
- firebase authentication, hosting, database, and storage with angularFire2
- expanded scss folder with helper classes for managing photos and layering components
- angular flex layout and angular material with responsive typography, theming, and variables for all material design colors
- file structure based off of the guidelines in the Ultamite Angular course
- simple permissions system that leverages firebase functions and interfaces with firebase authentication
- re designed angular router via ngrx/router-store
- common observable based ui helpers such as back/forward button functionality and loading/not loading timers.  
- heavy use of barrels to make expanding/re structuring the appication as painless as possible
- completely seperate file structures for the front end and back end(labeled 'client' and 'serverless')
- lazy loading for all feature modules
- 0% test coverage on default features.  TODO.  

This can be used to make basic brand websites as well, but it is a bit overkill for a billboard site.  

# Warnings:
 - If you do not have a fairly in depth understanding of NGRX, Angular, Typescript, and Firebase it will be difficult to use/understand this seed.
 - The seeds usefulness is not just in it's boilerplate code, it's in the conventions it requires.  If you do not make an effort to understand where certain files should be created and what operations should be done in each file/folder, you are missing out on the seeds main purpose and it's most useful feature
 - this applications default boilerplate has no test coverage

# Note:  You must do some configuration before the project will initially compile
 - You must add a credentials.ts file into the environments folder.  Note:  This file is ignored by git.  This File should have the firebase config for a web environment
 - You must replace all instances of 'seed.'  Usually this is simply replacing the title of the project, but the case requirements are often different

# Conventions when using the seed:
- component folder contains components
- services folder contains services that only perform simple crud operations
- store folder contains sub folders for ngrx actions, reducers, effects, and selectors
- models folder contains typescript interfaces used for type checking
- all data handling is observable based
- the services receive data from external API's as observables
- NGRX effects call the services and transform the data as needed using rxjs operators
- If you are handling data from an external API, use NGRX effects
- NGRX reducers are what change the local application state
- If you are changing the local application state, use a reducer
- NGRX actions are dispatched inside of component files and initiate effects/reducers
- NGRX selectors are used inside of components in order to access the data in the application state
- importing throughout the application is standard.  import * as SINGULAR-PARENT-FOLDER from 'lowest barrel link'.  example:  `import * as Model from '../../models'`  
- Global stuff such as UI and routing goes inside of the 'app' folder under 'angular.'
- feature folders use export things as forChild and are lazy loaded inside of the routes file inside of the app folder.

# How to Use
- expand the current features by adding subfiles throughout the file structure.  for example, if I wanted to expand the permissions feature to takes specific keys to view each page, I would add a keys.model.ts to the models folder, a keys.service.ts to the services folder, a keys.actions.ts to the actions folder, etc and then export them all from the barrels.  
- if you need a completely new feature, add a feature folder and use the same structure/conventions as the others.

- The 'User' Folder contains a basic system for auth, event/scheduling, chat, and profile.  These are specifically created to be general and to be changed based on the project.  
- if you need a completely new feature, add a feature folder and use the same structure/conventions as the others. 

# Required Technologies
- node
- firebase tools
- angular cli
- typescript
- git
