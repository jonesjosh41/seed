import * as creds from './credentials';

export const environment = {
  production: true,
  firebase: creds.firebase,
};
