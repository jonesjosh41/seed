# Architecture is meant to be expanded, but has some initial boilerplate.

The applications code is under 'src/angular' instead of the normal 'src/app.'  This is to keep the architecture consistent in terms of modular pieces of the application.  Under angular, each folder holds a different chunk of the application

- app is what is initially bootstrapped and contains the minimum amount necessary that the app requires globally.  ex:  header/footer/home, routing state

- main holds the applications pages that require no permissions and can be viewed by anyone.  In a brand website, this would hold the majority of the code.  This has some boilerplate already set up, since most websites all have very similar informational pages.  ex:  FAQ, about us, team, etc.

- shared contains code that is used globally in the application.  
