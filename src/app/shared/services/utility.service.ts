import { Injector, Injectable } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";

@Injectable({
    providedIn: 'root'
})

export class UtilityService {
    constructor(private injector: Injector) { }
    /**
     * Converts an object to a parametrised string
     * @param object
     * @returns {string}
     */
    static objectToParams(object): string {
        return Object.keys(object).map((key) => this.isJsObject(object[key]) ?
            this.subObjectToParams(encodeURIComponent(key), object[key]) :
            `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`
        ).join('&');
    }

    /**
     * Converts a sub-object to a parametrised string
     * @param object
     * @param key
     * @returns {string}
     */
    static subObjectToParams(key, object): string {
        return Object.keys(object).map((childKey) => this.isJsObject(object[childKey]) ?
            this.subObjectToParams(`${key}[${encodeURIComponent(childKey)}]`, object[childKey]) :
            `${key}[${encodeURIComponent(childKey)}]=${encodeURIComponent(object[childKey])}`
        ).join('&');
    }

    /**
     * Returns a boolean value based on if the passed in property is a valid javascript object.
     * @param object 
     * @returns {boolean}
     */
    static isJsObject(object: any): boolean {
        return object !== null && (typeof object === 'function' || typeof object === 'object');
    }

    /**
     * Reachs out via injector and returns a reference to the DOM document object.
     * @returns {Document}
     */
    public getDocument(): Document {
        return this.injector.get(DOCUMENT);
    }

    /**
     * Returns the current users viewport height based on the 
     * relative size of browser at the time of fn call
     * @returns {number}
     */
    calculateViewportHeight = (): number => {
        const body = this.getDocument().body,
            html = this.getDocument().documentElement;
        return Math.max(body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight);
    }

    /**
     * Returns the current users viewport width based on the 
     * relative size of browser at the time of fn call
     * @returns {number}
     */
    calculateViewportWidth = (): number => {
        const body = this.getDocument().body,
        html = this.getDocument().documentElement;
        return Math.max(body.scrollWidth, body.offsetWidth,
            html.clientWidth, html.scrollWidth, html.offsetWidth);
    }

    /**
     * Converts a string of text and returns the string in Title Casing 
     * @param text 
     * @returns {string} converted string in title casing
     */
    public static toTitleCase(text: string) {
        return !!text && text.length ? text.replace(/\w\S*/g, (subString) => {
            return subString.charAt(0).toUpperCase() + subString.substr(1).toLowerCase();
        }) : '';
    }

    /**
     * Retrieve the baseURL of the application in the current running instance
     * @returns {string} The Applications BaseURL
     */
    public static baseUrl(): string {
        let base = '';

        if (window.location.origin)
            base = window.location.origin;
        else
            base = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');

        return base.replace(/\/$/, '');
    }

    /**
     * Takes a query paramaterized string and converts the url into an object and then returns 
     * the object with an array of key values
     * @param paramString 
     */
    public static getQueryParamsFromString(paramString: string) {
        if (!paramString)
            return null;

        let params: { [key: string]: string } = {};

        for (const param of paramString.split("&")) {
            let keyValue = this.splitInTwo(param, "=");
            params[keyValue.firstPart] = keyValue.secondPart;
        }
        return params;
    }

    /**
     * Takes a text string and a seperator and splits the string in to two parts based 
     * on the seperator returning an object with two properties
     * @param text 
     * @param separator 
     * @returns {Object} { firstPart: string, secondPart: string }
     */
    public static splitInTwo(text: string, separator: string): { firstPart: string, secondPart: string } {
        let separatorIndex = text.indexOf(separator);

        if (separatorIndex == -1)
            return { firstPart: text, secondPart: null };

        let part1 = text.substr(0, separatorIndex).trim();
        let part2 = text.substr(separatorIndex + 1).trim();

        return { firstPart: part1, secondPart: part2 };
    }
}