import * as moment from 'moment';
import { Injectable } from '@angular/core';

export class CacheObject {
    value: any
    expirationDateTime: any
    constructor(_value: any, _expirationDateTime: any) {
        this.value = _value;
        this.expirationDateTime = _expirationDateTime;
    }
}

@Injectable()
export class BrowserCacheService {
    constructor() { }
    storage = window.localStorage;
    
    /**
     * Gets state of an item that is in the browsers cache
     * if the item doesn't exist or the time to live has expired,
     * this method returns null.
     * @param key string
     * @param value any
     * @param timeToLiveMilliseconds optional
     * @returns {void} void 
     */
    saveState = (key: string, value: any, timeToLiveMilliseconds?: number): void => {
        try {
            if (timeToLiveMilliseconds && timeToLiveMilliseconds > 0) {
                this.storage.setItem(key, JSON.stringify(new CacheObject(value, moment().millisecond(timeToLiveMilliseconds))));
            } else {
                this.storage.setItem(key, JSON.stringify(new CacheObject(value, null)));
            }
        } catch {
            // just protecting code here; nothing to do if local storage fails
        }
    }

    /**
     * Gets state of an item that is in the browsers cache
     * if the item doesn't exist or the time to live has expired,
     * this method returns null.
     * @param key 
     * @returns {CacheObject} CacheObject 
     * @returns {null} null
     */
    getState = (key: string) => {
        let data: CacheObject;
        if (this.storage[key]) {
            try {
                data = JSON.parse(this.storage.getItem(key));
                if (!!data.expirationDateTime && data.expirationDateTime instanceof moment
                    && data.expirationDateTime.isBefore(moment())) {
                    this.storage.removeItem(key);
                    return null;
                } else {
                    return (data.value);
                }
            } catch {
                return null;
            }
        }
        return null;
    }

    /**
     * Deletes state of an item that is in the browsers cache
     * @param key 
     * @returns {void} void
     */
    deleteState = (key: string) => {
        this.storage.removeItem(key);
    }

    /**
     * Clears state of the entire browsers cache
     * @returns {void} void
     */
    clearState = () => {
        this.storage.clear();
    }
}