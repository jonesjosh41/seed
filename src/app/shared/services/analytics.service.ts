import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  /**
   * sendAnalyticsEvent
   * @description Sends google analytics event
   * @param  {string} eventCategory
   * @param  {string} eventAction
   * @param  {string} label
   * @param  {string} eventValue? - Optional
   */
  public sendAnalyticsEvent(
    eventCategory: string,
    eventAction: string,
    eventLabel: string,
    eventValue?: string
  ): void {

    // Build the payload && attempt to send it.
    const eventPayload: EventPayload = { eventCategory, eventAction, eventLabel, eventValue };
    if (typeof window !== 'undefined') {
      (<any>window).ga('send', 'event', eventPayload);
    }
  }

  /**
   * Shortcut function for sending Navbar events
   * @description Sends google analytics event
   *
   * Category - NavBar
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendNavBarLinkEvent(label: string) {
    this.sendAnalyticsEvent('NavBar', 'LinkClick', label);
  }

  /**
   * Shortcut function for sending Carousel events
   * @description Sends google analytics event
   *
   * Category - Carousel
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendCarouselEvent(action: string, label: string): void {
    this.sendAnalyticsEvent('Carousel', action, label);
  }

  /**
   * Shortcut function for sending Footer events
   * @description Sends google analytics event
   *
   * Category - Footer
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendFooterEvent(label: string): void {
    this.sendAnalyticsEvent('Footer', 'LinkClick', label);
  }

  /**
   * Shortcut function for sending Order Options events
   * @description Sends google analytics event
   *
   * Category - Order Options
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendOrderOptionsEvent(label: string): void {
    this.sendAnalyticsEvent('OrderOptions', 'LinkClick', label);
  }

  /**
   * Shortcut function for sending Package Detail events
   * @description Sends google analytics event
   *
   * Category - Package Detail
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendPackageDetailEvent(action: string, label: string): void {
    this.sendAnalyticsEvent('PackageDetail', action, label);
  }

  /**
   * Shortcut function for sending Contact Copy events
   * @description Sends google analytics event
   *
   * Category - Contact Copy
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendContactCopyEvent(label: string): void {
    this.sendAnalyticsEvent('ContactCopy', 'LinkClick', label);
  }

  /**
   * Shortcut function for sending Package Addons events
   * @description Sends google analytics event
   *
   * Category - Package Addons
   *
   * @param  {string} action
   *
   * @param  {string} label
   */
  public sendPackageAddonsEvent(action: string, label: string): void {
    this.sendAnalyticsEvent('PackageAddons', action, label);
  }

  /**
   * Shortcut function for sending Mouse Selection events or a custom event type
   * @description Sends google analytics event
   *
   * Category - Game Picker
   *
   * @param  {string} action
   *  
   * @param  {string} label
   * 
   * @param  {string} type OPTIONAL, DEFAULTS TO Selection
   */
  public sendGamePickerEvent(action: string, label: string)
  public sendGamePickerEvent(action: string, label: string, type?: string): void {
    this.sendAnalyticsEvent(type || 'Selection', action, label);
  }

  /**
   * Shortcut function for sending Game Day events
   * @description Sends google analytics event
   *
   * Category - Game Day
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendGameDayEvent(action: string, label: string): void {
    this.sendAnalyticsEvent('GameDay', 'LinkClick', label);
  }

  /**
   * Shortcut function for sending Catering events
   * @description Sends google analytics event
   *
   * Category - Catering
   *
   * Action - LinkClick
   *
   * @param  {string} label
   */
  public sendCateringEvent(label: string): void {
    this.sendAnalyticsEvent('Catering', 'LinkClick', label);
  }
}

// TODO: Move this to an interface
export interface EventPayload {
  eventCategory: string;
  eventAction: string;
  eventLabel: string;
  eventValue?: string;
}