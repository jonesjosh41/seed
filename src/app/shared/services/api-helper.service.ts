import { HttpResponseBase, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UtilityService as Utility } from "./utility.service";

@Injectable()
export class ApiHelperService { 
    
    /**
     * Returns the body of the passed in http response / error if one occured
     * @param response 
     * @returns {HttpResponse<any>} HTTP Responses body or error if one occured
     */
    public static getResponseBody(response: HttpResponseBase): HttpResponse<any> {
        if (response instanceof HttpResponse)
            return response.body;

        if (response instanceof HttpErrorResponse)
            return response.error || response.message || response.statusText;
    }

    /**
     * Checks if the passed in HTTP Response returned a (0) status indicating
     * network access or not
     * @param response 
     * @returns {boolean}  Boolean value based on if the http response status returned (0) with no network access  
     */
    public static checkNoNetwork(response: HttpResponseBase): boolean {
        if (response instanceof HttpResponseBase) {
            return response.status === 0;
        }

        return false;
    }

    /**
     * Check if the passed in HTTP Response returned a 403 status of access denied
     * @param response 
     * @returns {boolean} Boolean value based on if the http response status was (403) access denied
     */
    public static checkAccessDenied(response: HttpResponseBase) {
        if (response instanceof HttpResponseBase) {
            return response.status == 403;
        }

        return false;
    }

    /**
     * Check if the passed in HTTP Response returned a 404 status of not found
     * @param response 
     * @returns {boolean} Boolean value based on if the http response status was (404) not found
     */
    public static checkNotFound(response: HttpResponseBase) {
        if (response instanceof HttpResponseBase) {
            return response.status === 404;
        }

        return false;
    }

    /**
     * Checks if the current environment url and base passed in resolve to a dev environment
     * (localhost or 127.0.0.1)
     * @param url 
     * @param base 
     * @returns {Boolean} Boolean value based on if the passed in url and base resolve to equal a localhost instance
     */
    public static checkIsLocalHost(url: string, base?: string) {
        if (url) {
            let location = new URL(url, base);
            return location.hostname === "localhost" || location.hostname === "127.0.0.1";
        }

        return false;
    }


    /**
     * Takes a query paramaterized string and converts the url into an object and then returns 
     * the object with an array of key values
     * @param paramString 
     */
    public static getQueryParamsFromString(paramString: string) {
        if (!paramString)
            return null;

        let params: { [key: string]: string } = {};

        for (const param of paramString.split("&")) {
            let keyValue = Utility.splitInTwo(param, "=");
            params[keyValue.firstPart] = keyValue.secondPart;
        }
        return params;
    }
}