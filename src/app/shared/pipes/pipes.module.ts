import { NgModule} from '@angular/core';
import { FormatPipe } from './formatPipe';
import { SafeHtmlPipe } from './safeHtml.pipe';

/**
 * Array of pipes for global reusage
 * add to this array as we add more pipes
 */
const JC_PIPES = [
    FormatPipe,
    SafeHtmlPipe
];

@NgModule({
    declarations: [JC_PIPES],
    exports: [JC_PIPES]
})
export class PipesModule { }