import { trigger, animate, transition, style, state, keyframes } from '@angular/animations';

export const slideInOutAnimation =
    trigger('slideInOutAnimation', [

        state('shown', style({
            transform: 'translate(0%)',
        })),

        transition(':enter', [
            style({ transform: 'translate(100%)'}),
            animate('200ms')
        ]),

        transition(':leave', [
            style({ transform: 'translate(-100%)'}),
            animate('200ms')
        ])
    ]);
