import { trigger, animate, transition, style, query, state, group } from '@angular/animations';

export const routingAnimation =

    trigger('routingAnimation', [

        transition( '* => *', [

            query(':enter', // hide the new page
                style({ opacity: 0, }),
                { optional: true }
            ),

            query(':leave', // animate the current page away
                [
                    style({ opacity: 1 }),
                    animate('0.2s', style({ opacity: 0 })),
                ], { optional: true }
            ),

            query(':enter', // animate in the new page
                [
                    style({ opacity: 0 }),
                    animate('0.2s', style({ opacity: 1 })),
                ], { optional: true }
            ),
        ])
    ]);
