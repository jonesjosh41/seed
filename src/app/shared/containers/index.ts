export * from './container/container.component';

import { ContainerComponent } from './container/container.component';

export const containers = [
    ContainerComponent,
];
