import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

/* RBC Forms */
import { FieldConfig } from '@rbcForms/models';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
  @Input() views: String[];

  @Output() currentView = new EventEmitter<string>();

  viewTypeField: FieldConfig = {
    formControlName: 'currentView',
    componentType: 'select',
    value: 'Month',
    label: 'Current View',
    selectOptions: [ ...this.views ],
    displayConfig: {
        size: '100%',
        appearance: 'outline',
        marginTop: '5%',
    },
    extras: {
        help: 'change the view'
    }
  };

  viewSelectForm: FieldConfig[] = [ this.viewTypeField ];

  constructor() { }

  ngOnInit() {
  }

  changeView(value) {
    this.currentView.emit(value.get('currentView').value);
  }

}
