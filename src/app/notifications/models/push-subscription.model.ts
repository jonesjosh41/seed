export interface IPushSubscription {
    /**
     * @description
     *  This contains a unique URL to a Firebase Cloud Messaging endpoint. 
     *  This url is a public but unguessable endpoint to the Browser Push Service 
     *  used by the application server to send push notifications to this subscription
     */
    endpoint: string;
    /**
     * @description
     * some messages are time sensitive and don't need to be sent if a certain time interval has passed.
     * This is useful in certain cases, for example, if a message might contain an authentication code 
     * that expires after 1 minute
     */
    expirationTime: Date | string;

    /**
     * @description
     * The Keys Object Holds two properties, a p256dh representing our encryption key the server utilizes, 
     * and an authentication secret
     */
    keys: {
        /**
         * @description
         * this is an encryption key that our server will use to encrypt the message, before sending it to the Push Service
         */
        p256dh: string,
        /**
         * @description
         * this is an authentication secret, which is one of the inputs of the message content encryption process
         */
        auth: string
    };
}