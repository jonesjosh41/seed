import { BehaviorSubject, from, pipe, Observable, Subscriber } from "rxjs";
import { Injectable } from "@angular/core";


@Injectable()
export class PushSubscriptionService {

    pushSubscriberSubject$ = new BehaviorSubject<PushSubscription[]>
        (new Array<PushSubscription>());
    pushSubscribers$ = this.pushSubscriberSubject$.asObservable();
    /**
     * Add a user to subscribe and receive push notifications
     * from the application on their device
     * @param sub 
     * @description
     * Appends the subscription passed into the existing push subscribers
     */
    addPushSubscriber(sub: PushSubscription) {
        this.pushSubscriberSubject$.next(
            [
                ...this.pushSubscriberSubject$.getValue(),
                { ...sub }
            ]);
    }

    ngOnInit() {
        console.dir(this.pushSubscriberSubject$);
    }

    ngOnDestroy() {
        this.pushSubscriberSubject$.next([]);
    }

    get pushSubscriptions(): Observable<any[]> {
        return from(this.pushSubscribers$);
    }
}


