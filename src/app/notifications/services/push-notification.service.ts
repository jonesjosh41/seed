import { Observable } from "rxjs";
import { Injectable, OnInit } from "@angular/core";
import * as Models from "../models";

@Injectable()
export class PushNotificationsService implements OnInit {
    public permission: Models.PushSubscriptionResult;

    constructor() {
    }

    public get isSupported(): boolean {
        return 'Notification' in window || (!!('Notification' in (<any>window)) || (!!(<any>window).Notification));
    }

    requestPermission(): void {
        let self = this;
        if (this.isSupported) {
            Notification.requestPermission(function (status) {
                return self.permission = status;
            });
        }
    }

    ngOnInit() {
        // set up the initial permission to default if we have access to 
        // the push notification object in the window
        this.permission = this.isSupported ?
            <Models.PushSubscriptionResult>'default'
            : <Models.PushSubscriptionResult>'denied';
    }
        
 

    initialize(): void {


  }



    /**
     * Create a push notification observable,
     * returning a new Observable that is subscribed too in the generateNotifcation function
     * 
     * @param title 
     * @param options 
     * @returns {Observable<Object>}
     */
    create(title: string, options?: Models.IPushNotification): Observable<Object> {

        return new Observable(obs => {
            if (!this.isSupported) {
                console.log('Notifications are not available in this environment');
                obs.complete();
            }

            if (this.permission !== 'granted') {
                console.warn("The user hasn't granted you permission to send push notifications.");
                obs.complete();
            }

            const Notify = new Notification(title, options);

            Notify.onshow = (e: Event) => {
                e.preventDefault();
                return obs.next({
                    notification: Notify,
                    event: e
                });
            };

            Notify.onclick = (e: Event) => {
                e.preventDefault();
                return obs.next({
                    notification: Notify,
                    event: e
                });
            };

            Notify.onerror = (e: Event) => {
                e.preventDefault();
                return obs.error({
                    notification: Notify,
                    event: e
                });
            };

            Notify.onclose = (e: Event) => {
                e.preventDefault();
                return obs.complete();
            };

        });
    }

    /**
     * Generates a push notification to current push subscribers
     * taking a source array and pushing a notification for each in the array
     * to all subscribers
     * @param source 
     * Array of objects in the structure of 
     * 
     *              { 
     * 
     *                  alertContent: string;
     * 
     *                  title: string;
     * 
     *              }
     */
    generateNotification(source: Array<any>): void {
        source.map((item) => {
            const options = {
                body: item.alertContent || 'hey',
                icon: "src/assets/icons/icon-72x72."
            };
            return this.create(item.title || 'Push Notification', options)
                .subscribe(sub => !!sub)
                .unsubscribe();
        });

    }
}