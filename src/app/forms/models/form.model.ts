import { ValidatorFn, FormGroup } from '@angular/forms';


export interface Field {
    config: FieldConfig;
    displayConfig?: DisplayConfig;
    inputConfig?: InputConfig;
    extras?: Extras;
    group: FormGroup;
}

export interface FieldConfig {
    formControlName: string; // form control name
    componentType: componentType; // form component type
    disabled?: boolean;
    required?: boolean;
    value?: any;
    label: string;
    selectOptions?: any[]; // options for a select component
    validators?: ValidatorFn[];
    errors?: FieldError[];
    crossFieldValidators?: ValidatorFn[];
    crossFieldErrors?: FieldError[];
    displayConfig?: DisplayConfig; // display properties
    inputConfig?: InputConfig; // extra properties for form components with an input(most of them)
    extras?: Extras; // misc extra things to add to forms
}

/* export interface FieldConfig {
    field: BaseConfig;
    display: DisplayConfig; // display properties
    input: InputConfig; // extra properties for form components with an input(most of them)
    error: ErrorConfig; // error handling config
    extras: Extras; // misc extra things to add to forms
} */

export interface DisplayConfig {
    size?: sizeType; // string of a number between 1 and 100 representing flex percentage
    appearance?: appearanceType; // mat-form-field input: 'legacy' | 'outline' | 'standard' | 'fill'
    labelPosition?: 'before' | 'after';
    marginRight?: string;
    marginLeft?: string;
    marginBottom?: string;
    marginTop?: string;
    mobile?: boolean;
}

export interface ErrorConfig {
    validators: ValidatorFn[];
    errors: FieldError[];
    crossFieldValidators?: ValidatorFn[];
    crossFieldErrors?: FieldError[];
}

export interface InputConfig {
    type?: inputType;
    required?: boolean;
    disabled?: boolean;
    autoCompleteOptions?: any[];
}

export interface Extras {
    prefixIcon?: string;
    prefixCustom?: any;
    suffixIcon?: string;
    suffixCustom?: string;
    hint?: string;
    help?: string;
}

export interface FieldError {
    validatorProperty: string;
    message: string;
}
export interface FieldHint {
    align: string;
    message: string;
}

export type componentType = 'color' | 'input' | 'textarea' | 'select' | 'checkbox' | 'date';
// supported component types

export type inputType = 'color' | 'date' | 'datetime-local' |
                 'email' | 'month' | 'number' |
                 'password' | 'search' | 'tel' |
                 'text' | 'time' | 'url' |
                 'week';
// supported input types

export type appearanceType = 'legacy' | 'outline' | 'standard' | 'fill';
// this is a config for mat-form-field that has default displays.  the config only accepts those 4 strings

export type sizeType = '100%' | '47%' | '30%' | '20%' | '17%' | '14%';
// each size is a flex value equaling a percentage of the current viewport
// there is a percentage for full, 1/2, 1/3, 1/4, 1/5, 1/6
// the reason the percentages do not evenly divide by 100 is because there is a 1% margin on both sides of every field
