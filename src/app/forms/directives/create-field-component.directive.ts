import {
    Directive,
    Input,
    ViewContainerRef,
    OnInit,
    ComponentFactoryResolver,
    ComponentRef,
    OnChanges,
    Type,
} from '@angular/core';

import { FormGroup } from '@angular/forms';

import {
    FormCheckBoxComponent,
    FormInputComponent,
    FormDatePickerComponent,
    FormSelectComponent,
    FormTextAreaComponent,
    FormColorPickerComponent
} from '../components';

import { Field, FieldConfig } from '../models';

const components: {[type: string]: Type<Field>} = {
    input:  FormInputComponent,
    date: FormDatePickerComponent,
    select: FormSelectComponent,
    textarea: FormTextAreaComponent,
    checkbox: FormCheckBoxComponent,
    color: FormColorPickerComponent,
};

@Directive({
  selector: '[appCreateFieldComponent]'
})
export class CreateFieldComponentDirective implements OnInit, OnChanges {
  component: ComponentRef<Field>;
  @Input() config: FieldConfig;
  @Input() group: FormGroup;

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef,
  ) {}

  ngOnChanges() {
    if (this.component) {
        this.component.instance.config = this.config;
        this.component.instance.displayConfig = this.config.displayConfig;
        this.component.instance.inputConfig = this.config.inputConfig;
        this.component.instance.extras = this.config.extras;
        this.component.instance.group = this.group;
    }
  }

  ngOnInit() {
    if (!components[this.config.componentType]) {
        const supportedTypes = Object.keys(components).join(', ');
        throw new Error(
            `Trying to use an unsupported type (${this.config.componentType}).
            Supported types: ${supportedTypes}`
        );
    }

    const component = this.resolver.resolveComponentFactory<Field>(components[this.config.componentType]);
    this.component = this.container.createComponent(component);

    this.component.instance.config = this.config;
    this.component.instance.displayConfig = this.config.displayConfig;
    this.component.instance.inputConfig = this.config.inputConfig;
    this.component.instance.extras = this.config.extras;
    this.component.instance.group = this.group;
  }
}
