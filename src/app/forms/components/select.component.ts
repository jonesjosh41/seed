import { Component, HostBinding, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldConfig, Field, DisplayConfig, InputConfig, Extras } from '../models';

@Component({
  selector: 'app-select',
  template: `
  <span>
  <mat-form-field [formGroup]="group" [style.width]="'100%'" [appearance]="appearance">

    <!-- Potential Prefix -->
    <span matPrefix *ngIf="customPrefix">{{ config.prefix.custom }}</span>
    <mat-icon matPrefix *ngIf="iconPrefix">{{ config.prefix.icon }}</mat-icon>

            <mat-select
                [required]="required"
                [placeholder]="placeholder"
                [formControlName]="config.formControlName">
                <mat-option *ngFor="let option of selectOptions" [value]="option">
                    {{ option }}
                </mat-option>
            </mat-select>

      <!-- Handle Field Specific Errors -->
      <ng-container *ngFor="let error of errors" ngProjectAs="mat-error">
        <mat-error fxFlex *ngIf="group.get(config.formControlName).hasError(error.validatorProperty)">
          {{ error.message }}
        </mat-error>
      </ng-container>

      <!-- Handle Cross field errors -->
      <ng-container *ngFor="let error of crossFieldErrors" ngProjectAs="mat-error">
        <mat-error fxFlex *ngIf="group.hasError(error.validatorProperty)">
          {{ error.message }}
        </mat-error>
      </ng-container>

      <!-- Potential Suffix -->
      <span *ngIf="customSuffix" matSuffix>{{ config.suffix.custom }}</span>
      <mat-icon *ngIf="iconSuffix" matSuffix>{{ config.suffix.icon }}</mat-icon>

      <!-- Potential Hint -->
      <mat-hint *ngIf="hint"> {{ config.extras.hint }}</mat-hint>

      <!-- Potential Help Tooltip -->
      <button mat-icon-button
          *ngIf="help && !valid"
          matSuffix
          [matTooltip]="config.extras.help"
          aria-label="button that displays a tooltip with additional info when focused or hovered over"
          color="primary">
        <mat-icon>help</mat-icon>
      </button>

    </mat-form-field>
    </span>
  `,
  styles: [`

  `],
})
export class FormSelectComponent implements Field {
  @Input() group: FormGroup;
  @Input() config: FieldConfig;
  @Input() inputConfig: InputConfig;

  @Input() set displayConfig(display: DisplayConfig) {
    this.flex = !!display ? display.size : '100%';
    this.marginLeft = !!display ? display.marginLeft : '1%';
    this.marginRight = !!display ? display.marginRight : '1%';
    this.marginTop = !!display ? display.marginTop : '0%';
    this.marginBottom = !!display ? display.marginBottom : '0%';
  }
  @Input() extras: Extras;

  @HostBinding('style.flex') public flex;
  @HostBinding('style.marginLeft') public marginLeft;
  @HostBinding('style.marginRight') public marginRight;
  @HostBinding('style.marginBottom') public marginBottom;
  @HostBinding('style.marginTop') public marginTop;

  // Input Configs
  get autocomplete() { return this.inputConfig.autoCompleteOptions; }
  get required() { return !!this.inputConfig ? this.inputConfig.required : false; }
  get type() { return this.inputConfig.type; }
  get placeholder() { return this.config.label; }
  get selectOptions() { return !!this.config ? this.config.selectOptions : []; }
  
  // display
  get mobile() { return this.displayConfig.mobile; }
  get appearance() { return !!this.displayConfig ? this.displayConfig.appearance : ''; }

  // extras
  get hint() { return !!this.extras ? this.extras.hint : ''; }
  get help() { return !!this.extras ? this.extras.help : ''; }
  get customPrefix() { return !!this.extras ? this.extras.prefixCustom : ''; }
  get iconPrefix() { return !!this.extras ? this.extras.prefixIcon : ''; }
  get customSuffix() { return !!this.extras ? this.extras.suffixCustom : ''; }
  get iconSuffix() { return !!this.extras ? this.extras.suffixIcon : ''; }

  // errors
  get errors() { return this.config.errors; }
  get crossFieldErrors() { return this.config.crossFieldErrors; }

  // form control stuff
  get valid() { return this.group.get(`${this.config.formControlName}`).valid; }
  get value() { return this.group.get(`${this.config.formControlName}`).value; }
}
