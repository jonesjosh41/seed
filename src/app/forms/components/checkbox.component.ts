import { Component, HostBinding, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldConfig, Field, DisplayConfig, InputConfig, Extras } from '../models';

@Component({
  selector: 'app-checkbox',
  template: `
    <mat-checkbox
        [labelPosition]="labelPosition"
        [disabled]="disabled">
            {{ placeholder }}
    </mat-checkbox>
  `,
  styles: [`
    ::ng-deep .mat-checkbox .mat-checkbox-frame {
        border-color: blue !important;
        background-color: blue !important;
        color: white !important;
    }
  `],
})
export class FormCheckBoxComponent implements Field {
  @Input() group: FormGroup;
  @Input() config: FieldConfig;
  @Input() inputConfig: InputConfig;
  @Input() displayConfig: DisplayConfig;
  @Input() extras: Extras;

  get value() { return this.group.get(`${this.config.formControlName}`).value; }
  get disabled() { return this.config.disabled; }
  get placeholder() { return this.config.label; }
  get labelPosition() { return this.displayConfig.labelPosition; }
}
