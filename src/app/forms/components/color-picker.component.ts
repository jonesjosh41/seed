import { Component, HostBinding, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldConfig, Field, DisplayConfig, InputConfig, Extras } from '../models';

@Component({
  selector: 'app-color',
  template: `
  <span fxLayout [formGroup]="group">
              <input
                  fxFlex
                  [cpFallbackColor]="'#eeeeee'"
                  [value]="color"
                  [(colorPicker)]="color"
                  [formControlName]="config.formControlName"
                  [style.background]="color"
                  [(cpToggle)]="toggle">

              <button mat-raised-button (click)="toggleCP()">Choose a Color</button>
  </span>
  `,
  styles: [`

  `],
})
export class FormColorPickerComponent implements Field {
  @Input() group: FormGroup;
  @Input() config: FieldConfig;
  @Input() inputConfig: InputConfig;
  @Input() set displayConfig(display: DisplayConfig) {
    this.flex = !!display ? display.size : '100%';
    this.marginLeft = !!display ? display.marginLeft : '1%';
    this.marginRight = !!display ? display.marginRight : '1%';
    this.marginTop = !!display ? display.marginTop : '0%';
    this.marginBottom = !!display ? display.marginBottom : '0%';
  }
  @Input() extras: Extras;

  @HostBinding('style.flex') public flex;
  @HostBinding('style.marginLeft') public marginLeft;
  @HostBinding('style.marginRight') public marginRight;
  @HostBinding('style.marginBottom') public marginBottom;
  @HostBinding('style.marginTop') public marginTop;
  color;
  toggle = false;

  toggleCP() {
    this.toggle = !this.toggle;
  }
  get size() { return this.displayConfig.size; }
  // Input Configs
  get autocomplete() { return this.inputConfig.autoCompleteOptions; }
  get required() { return this.inputConfig.required; }
  get type() { return this.inputConfig.type; }
  get placeholder() { return this.config.label; }

  // display
  get mobile() { return !!this.displayConfig ? this.displayConfig.mobile : true; }
  get appearance() { return !!this.displayConfig ? this.displayConfig.appearance : ''; }

  // extras
  get hint() { return !!this.extras ? this.extras.hint : ''; }
  get help() { return !!this.extras ? this.extras.help : ''; }
  get prefixCustom() { return !!this.extras ? this.extras.prefixCustom : ''; }
  get prefixIcon() { return !!this.extras ? this.extras.prefixIcon : ''; }
  get suffixCustom() { return !!this.extras ? this.extras.suffixCustom : ''; }
  get suffixIcon() { return !!this.extras ? this.extras.suffixIcon : ''; }

  // errors
  get errors() { return this.config.errors; }
  get crossFieldErrors() { return this.config.crossFieldErrors; }

  // form control stuff
  get valid() { return this.group.get(`${this.config.formControlName}`).valid; }
  get value() { return this.group.get(`${this.config.formControlName}`).value; }
}
