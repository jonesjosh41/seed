import { Component, HostBinding, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldConfig, Field, DisplayConfig, InputConfig, Extras } from '../models';

@Component({
  selector: 'app-date',
  template: `
  <span>
    <mat-form-field [formGroup]="group" [style.width]="'100%'" [appearance]="''">

      <!-- Potential Prefix -->
      <span matPrefix *ngIf="customPrefix">{{ customPrefix }}</span>
      <mat-icon matPrefix *ngIf="iconPrefix">{{ iconPrefix }}</mat-icon>

              <input
                  (click)="picker.open"
                  matInput
                  [disabled]="inputDisabled"
                  [matDatepicker]="picker"
                  [placeholder]="placeholder"
                  [formControlName]="config.formControlName">

              <mat-datepicker-toggle matPrefix [for]="picker" color="primary"></mat-datepicker-toggle>
              <mat-datepicker [touchUi]="mobile" #picker disabled="false"></mat-datepicker>
    </mat-form-field>
  </span>
  `,
  styles: [`

  `],
})
export class FormDatePickerComponent implements Field {
  @Input() group: FormGroup;
  @Input() config: FieldConfig;
  @Input() inputConfig: InputConfig;
  @Input() set displayConfig(display: DisplayConfig) {
    this.flex = !!display ? display.size : '100%';
    this.marginLeft = !!display ? display.marginLeft : '1%';
    this.marginRight = !!display ? display.marginRight : '1%';
    this.marginTop = !!display ? display.marginTop : '0%';
    this.marginBottom = !!display ? display.marginBottom : '0%';
  }
  @Input() extras: Extras;

  @HostBinding('style.flex') public flex;
  @HostBinding('style.marginLeft') public marginLeft;
  @HostBinding('style.marginRight') public marginRight;
  @HostBinding('style.marginBottom') public marginBottom;
  @HostBinding('style.marginTop') public marginTop;

  get size() { return this.displayConfig.size; }
  // Input Configs
  get autocomplete() { return this.inputConfig.autoCompleteOptions; }
  get required() { return this.inputConfig.required; }
  get type() { return this.inputConfig.type; }
  get placeholder() { return this.config.label; }

  // display
  get mobile() { return !!this.displayConfig ? this.displayConfig.mobile : true; }
  get appearance() { return !!this.displayConfig ? this.displayConfig.appearance : ''; }

  // extras
  get hint() { return !!this.extras ? this.extras.hint : ''; }
  get help() { return !!this.extras ? this.extras.help : ''; }
  get customPrefix() { return !!this.extras ? this.extras.prefixCustom : ''; }
  get iconPrefix() { return !!this.extras ? this.extras.prefixIcon : ''; }
  get customSuffix() { return !!this.extras ? this.extras.suffixCustom : ''; }
  get iconSuffix() { return !!this.extras ? this.extras.suffixIcon : ''; }

  // errors
  get errors() { return this.config.errors; }
  get crossFieldErrors() { return this.config.crossFieldErrors; }
  get inputDisabled() { return false; }
  // form control stuff
  get valid() { return this.group.get(`${this.config.formControlName}`).valid; }
  get value() { return this.group.get(`${this.config.formControlName}`).value; }
}
