import { Component, HostBinding, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FieldConfig, InputConfig, DisplayConfig, Extras, Field } from '../models';

@Component({
  selector: 'app-input',
  template: `
    <span>
    <mat-form-field [formGroup]="group" [style.width]="'100%'" [appearance]="''">

      <!-- Potential Prefix -->
      <span matPrefix *ngIf="customPrefix">{{ customPrefix }}</span>
      <mat-icon matPrefix *ngIf="iconPrefix">{{ iconPrefix }}</mat-icon>

      <input
        matInput
        [type]="type"
        [required]="required"
        [placeholder]="placeholder"
        [formControlName]="config.formControlName">

      <!-- Handle Field Specific Errors -->
      <ng-container *ngFor="let error of errors" ngProjectAs="mat-error">
        <mat-error fxFlex *ngIf="group.get(config.formControlName).hasError(error.validatorProperty)">
          {{ error.message }}
        </mat-error>
      </ng-container>

      <!-- Handle Cross field errors -->
      <ng-container *ngFor="let error of crossFieldErrors" ngProjectAs="mat-error">
        <mat-error fxFlex *ngIf="group.hasError(error.validatorProperty)">
          {{ error.message }}
        </mat-error>
      </ng-container>

      <!-- Potential Suffix -->
      <span *ngIf="customSuffix" matSuffix>{{ config.extras.customSuffix }}</span>
      <mat-icon *ngIf="iconSuffix" matSuffix>{{ config.extras.iconSuffix }}</mat-icon>

      <!-- Give Feedback based on form validity -->
      <button mat-icon-button matSuffix *ngIf="!valid && group.get(config.formControlName).touched">
          <mat-icon aria-label="form field is invalid" style="color:red">error</mat-icon>
      </button>
      <button mat-icon-button matSuffix *ngIf="valid && group.get(config.formControlName).dirty">
          <mat-icon aria-label="form field is valid" style="color:green">check_circle</mat-icon>
      </button>

      <!-- Potential Hint -->
      <mat-hint *ngIf="hint"> {{ hint }}</mat-hint>

      <!-- Potential Help Tooltip -->
      <button mat-icon-button
          *ngIf="help && !valid"
          matSuffix
          [matTooltip]="config.extras.help"
          aria-label="button that displays a tooltip with additional info when focused or hovered over"
          color="primary">
        <mat-icon>help</mat-icon>
      </button>

    </mat-form-field>
    </span>`,
  styles: [`
  `]
})
export class FormInputComponent implements Field {
  @Input() group: FormGroup;
  @Input() config: FieldConfig;
  @Input() inputConfig: InputConfig;

  @Input() set displayConfig(display: DisplayConfig) {
    this.flex = !!display ? display.size : '100%';
    this.marginLeft = !!display ? display.marginLeft : '1%';
    this.marginRight = !!display ? display.marginRight : '1%';
    this.marginTop = !!display ? display.marginTop : '0%';
    this.marginBottom = !!display ? display.marginBottom : '0%';
  }
  @Input() extras: Extras;

  @HostBinding('style.flex') public flex;
  @HostBinding('style.marginLeft') public marginLeft;
  @HostBinding('style.marginRight') public marginRight;
  @HostBinding('style.marginBottom') public marginBottom;
  @HostBinding('style.marginTop') public marginTop;

  // Input Configs
  get autocomplete() { return this.inputConfig.autoCompleteOptions; }
  get required() { return this.inputConfig.required; }
  get type() { return this.inputConfig.type; }
  get placeholder() { return this.config.label; }

  // display
  get mobile() { return this.displayConfig.mobile; }
  get appearance() { return this.displayConfig.appearance; }

  // extras
  get hint() { return !!this.extras ? this.extras.hint : ''; }
  get help() { return !!this.extras ? this.extras.help : ''; }
  get customPrefix() { return !!this.extras ? this.extras.prefixCustom : ''; }
  get iconPrefix() { return !!this.extras ? this.extras.prefixIcon : ''; }
  get customSuffix() { return !!this.extras ? this.extras.suffixCustom : ''; }
  get iconSuffix() { return !!this.extras ? this.extras.suffixIcon : ''; }

  // errors
  get errors() { return this.config.errors; }
  get crossFieldErrors() { return this.config.crossFieldErrors; }

  // form control stuff
  get valid() { return this.group.get(`${this.config.formControlName}`).valid; }
  get value() { return this.group.get(`${this.config.formControlName}`).value; }
}
