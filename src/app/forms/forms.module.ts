import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ColorPickerModule } from 'ngx-color-picker';

// components
import { formFieldComponents } from './components';

// containers
import { containers } from './containers';

// directives
import { directives } from './directives';

import {
    MatAutocompleteModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSortModule,
    MatStepperModule,
    MatToolbarModule,
    MatTooltipModule,
} from '@angular/material';

const materialComponents = [
    MatAutocompleteModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSortModule,
    MatStepperModule,
    MatToolbarModule,
    MatTooltipModule,
];

@NgModule({
    declarations: [
        ...directives,
        ...formFieldComponents,
        ...containers
    ],
    imports: [
        ...materialComponents,
        FlexLayoutModule,
        CommonModule,
        ReactiveFormsModule,
        ColorPickerModule,
    ],
    exports: [
        ...materialComponents,
        ...directives,
        ...formFieldComponents,
        ...containers,
        ReactiveFormsModule,
        FlexLayoutModule,
        CommonModule,
        ColorPickerModule,
    ],
})
export class RBCFormsModule { }
