export interface VideoFinal {
    id: string;
    title: string;
    description: string;
    loading: boolean;
    owner: string;
    publishedDate: string;
    category: string;
    views: number;
    tags: string[];
    keys: string[];
    comments: string[];
    url: string;
}

export interface Video {
    id: string;
    title: string;
    loading: string;
    owner: string;
    url: string;
}
