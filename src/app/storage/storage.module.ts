import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

/* NGRX */
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Routes
import { ROUTES } from './containers/routes';

// Components
import { components } from './components';

// Containers
import { containers } from './containers';

// Directives
import { directives } from './directives';

// Pipes
import { pipes } from './pipes';

// Shared
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    ...components,
    ...containers,
    ...directives,
    ...pipes,
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  entryComponents: [
  ]
})
export class UserModule { }
