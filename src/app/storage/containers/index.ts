import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import { UploadPageComponent } from './upload/upload.component';

export * from './item/item.component';
export * from './list/list.component';
export * from './upload/upload.component';

export const containers = [
    ListComponent,
    ItemComponent,
    UploadPageComponent
];
