import { DropZoneDirective } from './drop-zone.directive';

export * from './drop-zone.directive';

export const directives = [
    DropZoneDirective,
];
