import { FiltersComponent } from './filters/filters.component';
import { GalleryComponent } from './gallery/gallery.component';
import { OverlayComponent } from './overlay/overlay.component';
import { UploadFormComponent } from './upload-form/upload-form.component';

export * from './filters/filters.component';
export * from './gallery/gallery.component';
export * from './overlay/overlay.component';
export * from './upload-form/upload-form.component';

export const components = [
    FiltersComponent,
    GalleryComponent,
    OverlayComponent,
    UploadFormComponent
];
