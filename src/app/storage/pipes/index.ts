import { FileSizePipe } from './file-size.pipe';

export * from './file-size.pipe';

export const pipes = [
    FileSizePipe,
];
