import * as Router from '@ngrx/router-store';

import * as UserEffect from '@user/store/effects';
import * as CoreEffect from '@core/store/effects';

import * as CoreReducer from '@core/store/reducers';
import * as UserReducer from '@user/store/reducers';

import * as CoreModel from '@core/models';
import * as UserModel from '@user/models';

import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

// Model
export interface CoreState {
    routerState: Router.RouterReducerState<CoreModel.RouterStateUrl>;
    ui: CoreModel.UIState;
    auth: UserModel.AuthState;
    permissions: UserModel.Permissions;
}

// Reducer
export const reducers: ActionReducerMap<CoreState> = {
    routerState: Router.routerReducer,
    ui: CoreReducer.uiReducer,
    permissions: UserReducer.permissionsReducer,
    auth: UserReducer.authReducer,
};

// Effects
export const effects: any[] = [
    CoreEffect.UIEffects,
    CoreEffect.RouterEffects,
    UserEffect.PermissionsEffects,
    UserEffect.AuthEffects,
];

// Selector
export const getCoreState = createFeatureSelector<CoreState>('root');
