import * as RouterEffect from './router.effects';
import * as UIEffect from './ui.effects';
import * as PermissionsEffects from '../../../user/store/effects/permissions.effects';
import * as AuthEffects from '../../../user/store/effects/auth.effects';

export const effects: any[] = [
  RouterEffect.RouterEffects,
  UIEffect.UIEffects,
  PermissionsEffects.PermissionsEffects,
  AuthEffects.AuthEffects
];

export * from './ui.effects';
export * from './router.effects';

