import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Effect, Actions } from '@ngrx/effects';
import * as Action from '../actions';

import { tap, pluck, debounceTime } from 'rxjs/operators';

@Injectable()
export class UIEffects {

  constructor(
    private actions$: Actions,
    private snackbar: MatSnackBar
  ) {}

  @Effect( { dispatch: false })
  showSnackbar$ = this.actions$
    .ofType(Action.SHOW_SNACKBAR)
    .pipe(
      pluck('payload'),
      tap((payload: string) => this.snackbar.open(payload)),
      debounceTime(3000),
      tap(() => this.snackbar.dismiss())
    );

}
