import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';

import * as Model from '../../models';

export const getRouterFeatureState = createFeatureSelector<fromRouter.RouterReducerState<Model.RouterStateUrl>>('routerState');

export const getRouterState = createSelector(
  getRouterFeatureState,
  (router) => router.state
);

export const getRouterParamId = createSelector(
    getRouterState,
    (state): number => state.params.id
);

export const getPageVerb = createSelector(
    getRouterState,
    (state) => state.data.verb
);

export const getPageView = createSelector(
    getRouterState,
    (state): string => state.data.view
);

export const getPageClearance = createSelector(
    getRouterState,
    (state): number => state.data.clearance
);

export const getPageKeys = createSelector(
    getRouterState,
    (state): string[] => state.data.keys
);

export const getPageMeta = createSelector(
    getRouterState,
    (state): any => state.data.meta
);



