import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as Model from '../../models';

export const getUIState = createFeatureSelector<Model.UIState>('ui');

export const getMobileNavState = createSelector(
    getUIState,
    (state: Model.UIState) => state.mobileNavState
);

export const getLoading = createSelector(
    getUIState,
    (state: Model.UIState) => state.isLoading
);

export const getBackAvailable = createSelector(
    getUIState,
    state => state.enableForward
);

export const getForwardAvailable = createSelector(
    getUIState,
    state => state.enableForward
);

export const getViewGlobalButtonOptions = createSelector(
    getUIState,
    state => state.viewAdditionalOptions
);

export const getCurrentFeature = createSelector(
    getUIState,
    state => state.currentFeature
);


