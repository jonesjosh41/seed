import { Action } from '@ngrx/store';


export const START_LOADING = 'START_LOADING';
export const STOP_LOADING = 'STOP_LOADING';

export const CANT_GO_BACK = 'CANT_GO_BACK';
export const CANT_GO_FORWARD = 'CANT_GO_FORWARD';
export const CAN_GO_FORWARD = 'CAN_GO_FORWARD';
export const CAN_GO_BACK = 'CAN_GO_BACK';

export const SHOW_ADDITIONAL_OPTIONS = 'SHOW_ADDITIONAL_OPTIONS';
export const HIDE_ADDITIONAL_OPTIONS = 'HIDE_ADDITIONAL_OPTIONS';

export const SHOW_MOBILE_NAV = 'SHOW_MOBILE_NAV';
export const HIDE_MOBILE_NAV = 'HIDE_MOBILE_NAV';

export const SHOW_SNACKBAR = 'SHOW_SNACKBAR';

export const HIDE_BASE_NAVBAR = 'HIDE_BASE_NAVBAR';

export const SET_CURRENT_FEATURE = 'SET_CURRENT_FEATURE';

export class ShowSnackbar implements Action {
    readonly type = SHOW_SNACKBAR;
    constructor( public payload: string) {}
}

export class SetCurrentFeature implements Action {
    readonly type = SET_CURRENT_FEATURE;
    constructor( public payload: string) {}
}

export class StartLoading implements Action { readonly type = START_LOADING; }
export class StopLoading implements Action { readonly type = STOP_LOADING; }

export class CantGoBack implements Action { readonly type = CANT_GO_BACK; }
export class CantGoForward implements Action { readonly type = CANT_GO_FORWARD; }
export class CanGoBack implements Action { readonly type = CAN_GO_BACK; }
export class CanGoForward implements Action { readonly type = CAN_GO_FORWARD; }

export class ShowAdditionalOptions implements Action { readonly type = SHOW_ADDITIONAL_OPTIONS; }
export class HideAdditionalOptions implements Action { readonly type = HIDE_ADDITIONAL_OPTIONS; }

export class ShowMobileNav implements Action { readonly type = SHOW_MOBILE_NAV; }
export class HideMobileNav implements Action { readonly type = HIDE_MOBILE_NAV; }

export type UIActions =
| SetCurrentFeature
| ShowSnackbar
| ShowMobileNav
| HideMobileNav
| StartLoading
| StopLoading
| CantGoBack
| CantGoForward
| CanGoForward
| CanGoBack
| ShowAdditionalOptions
| HideAdditionalOptions;
