import { Params, NavigationExtras, Data } from '@angular/router';
import * as Router from '@ngrx/router-store';

export interface RouterStateUrl {
  url: string;
  component: any;
  queryParams: Params;
  params: Params;
  data: Data;
}

export interface RouterNavigation {
  path: any[];
  query?: object;
  extras?: NavigationExtras;
}

export interface RouterState {
  routerReducer: Router.RouterReducerState<RouterStateUrl>;
}

