import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { VisitorNavComponent } from './header/navigation/visitor.component';
import { MemberNavComponent } from './header/navigation/member.component';
import { AdminNavComponent } from './header/navigation/admin.component';
import { HomeComponent } from './home/home.component';
import { MobileNavComponent } from './mobile-nav/mobile-nav.component';
import { FeatureNavComponent } from './header/navigation/feature.component';
import { EmployeeNavComponent } from './header/navigation/employee.component';

export * from './header/header.component';
export * from './home/home.component';
export * from './footer/footer.component';
export * from './mobile-nav/mobile-nav.component';

export const containers = [
    FooterComponent,
    HeaderComponent,
    VisitorNavComponent,
    MemberNavComponent,
    AdminNavComponent,
    HomeComponent,
    MobileNavComponent,
    FeatureNavComponent,
    EmployeeNavComponent
];
