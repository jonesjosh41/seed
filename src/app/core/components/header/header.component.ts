import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import * as CoreAction from '@core/store/actions';
import * as CoreModel from '@core/models';
import * as UISelector from '@core/store/selectors/ui.selectors';
import * as PermissionsSelector from '@user/store/selectors/permissions.selector';

import * as UserAction from '@user/store/actions';
import * as UserModel from '@user/models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  forwardAvailable$: Observable<boolean>;
  backwardAvailable$: Observable<boolean>;
  currentFeature$: Observable<string>;
  mobileNavState$: Observable<string>;
  userClearance$: Observable<number>;
  userKeys$: Observable<string[]>;
  isLoading$: Observable<boolean>;

  toggleOptions:  boolean;

  constructor(
    private store: Store<any>,
  ) { }

  ngOnInit() {
    this.backwardAvailable$ = this.store.select(UISelector.getBackAvailable);
    this.forwardAvailable$ = this.store.select(UISelector.getForwardAvailable);
    this.mobileNavState$ = this.store.select(UISelector.getMobileNavState);
    this.userClearance$ = this.store.select(PermissionsSelector.getClearance);
    this.currentFeature$ = this.store.select(UISelector.getCurrentFeature);
    this.userKeys$ = this.store.select(PermissionsSelector.getKeys);
    this.isLoading$ = this.store.select(UISelector.getLoading);
    this.toggleOptions = false;
  }

  onBack() {
    this.store.dispatch(new CoreAction.Back);
  }

  onForward() {
    this.store.dispatch(new CoreAction.Forward);
  }

  onLogout() {
    this.store.dispatch(new UserAction.LogoutUser);
  }

  onMobileNavToggle() {
    if (this.toggleOptions === false) {
      this.store.dispatch(new CoreAction.ShowMobileNav);
      this.toggleOptions = true;
    } else {
        this.store.dispatch(new CoreAction.HideMobileNav);
        this.toggleOptions = false;
    }
  }

}
