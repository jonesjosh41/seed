import { Component } from '@angular/core';


@Component({
  selector: 'app-member-nav',
  template: `
  <div>
    <button class="button"
        mat-menu-item
        routerLinkActive="active"
        routerLink="/user/profile">
            Profile
    </button>
  </div>
  `,
  styles: [`
  `]
})
export class MemberNavComponent {

}


