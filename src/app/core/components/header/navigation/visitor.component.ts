import { Component } from '@angular/core';


@Component({
  selector: 'app-visitor-nav',
  template: `
  <div>
    <button class="button"
        mat-menu-item
        routerLinkActive="active"
        routerLink="/user/login">
            login
    </button>
    <button class="button"
        mat-menu-item
        routerLinkActive="active"
        routerLink="/user/register">
            register
    </button>
    </div>
  `,
  styles: [`
  `]
})
export class VisitorNavComponent {

}
