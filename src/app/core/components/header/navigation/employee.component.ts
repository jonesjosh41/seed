import { Component } from '@angular/core';


@Component({
  selector: 'app-employee-nav',
  template: `
    <button class="button"
        mat-menu-item
        routerLinkActive="active"
        routerLink="/user/login">
            login
    </button>
    <button class="button"
        mat-menu-item
        routerLinkActive="active"
        routerLink="/user/register">
            register
    </button>
  `,
  styles: [`
  `]
})
export class EmployeeNavComponent {

}
