import { Component } from '@angular/core';

@Component({
  selector: 'app-feature-nav',
  template: `
    <a *ngFor="let feature of features"
        mat-button
        class="button"
        routerLinkActive="active"
        routerLink="{{ feature.route }}">
      <mat-icon class="icon">{{ feature.icon }}</mat-icon>
      <p>{{ feature.name }}</p>
    </a>
  `,
  styles: [`
    .button {
      width: 90px;
    }
  `]
})
export class FeatureNavComponent {
  features = [
/*     {
      icon: 'dashboard',
      name: 'Dashboard',
    },
    {
      icon: 'build',
      name: 'Admin'
    }, */
    {
      icon: 'schedule',
      name: 'Create Event',
      route: '/events/create'
    },
    {
      icon: 'event',
      name: 'Calendar',
      route: '/events/calendar'
    },
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
/*     {
      icon: 'store',
      name: 'Store'
    },
    {
      icon: 'chat',
      name: 'Chat'
    },
    {
      icon: 'contacts',
      name: 'Contact'
    },
    {
      icon: 'help',
      name: 'FAQ'
    } */
  ];
}
