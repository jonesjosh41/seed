import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import { Store } from '@ngrx/store';

import * as Model from '../../models';
import * as Action from '../../store/actions';
import * as UISelector from '../../store/selectors/ui.selectors';
import * as Animation from '../../../shared/animations';
import { firestore } from 'firebase';
import { LogoutUser as LogoutAction } from '@user/store/actions';

@Component({
  selector: 'app-mobile-nav',
  animations: [ Animation.slideInOutAnimation ],
  templateUrl: './mobile-nav.component.html',
  styleUrls: ['./mobile-nav.component.scss']
})
export class MobileNavComponent implements OnInit {
  mobileNavState$: Observable<string>;
  userKeys$: Observable<any>;
  constructor(
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.mobileNavState$ = this.store.select(UISelector.getMobileNavState);
    //TODO: Fix this, not sure exactly what happens here
    //      for now returning empty observable to compile
    this.userKeys$ = from([]);
  }

  onClick() {
    this.store.dispatch(new Action.HideMobileNav);
  }

  onLogout() {
    this.store.dispatch(new LogoutAction());
  }
}
