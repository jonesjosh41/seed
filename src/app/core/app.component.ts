import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import * as UISelector from '@core/store/selectors/ui.selectors';
import * as UserStore from '@user/store';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, catchError, tap, filter } from 'rxjs/operators';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { PushSubscriptionService } from 'app/notifications/services/push-subscriptions.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  optionsShowing$: Observable<boolean>;
  mobileNavState$: Observable<string>;
  readonly VAPID_PUBLIC_KEY = 'BF6TD-0-cT73v7z16lROS_r9Zz85xQUFfs2Qys9OOtJSJTS-cZfGZ8Eip3_4wZi20a0qe132afC0XcRyQSo0bRU';

  constructor(
    private store: Store<any>,
    private afAuth: AngularFireAuth,
    private swPush: SwPush,
    private swUpdate: SwUpdate,
    public pushSubSvc: PushSubscriptionService
  ) {
  }

  ngOnInit() {
    this.optionsShowing$ = this.store.select(UISelector.getViewGlobalButtonOptions);
    this.mobileNavState$ = this.store.select(UISelector.getMobileNavState);
    this.afAuth.user.pipe(
      map(user => !!user ? user.uid : ''),
      tap(uid => {
        if (!!uid) {
          this.store.dispatch(new UserStore.SetUserID(uid));
          this.store.dispatch(new UserStore.GetUserPermissions(uid));
        }
      }),
      catchError(err => of(console.log(err)))
    ).subscribe(sub => !!sub)
      .unsubscribe();
    this.subscribeToNotifications();
  }

  /**
    * @description
    * Subscribe to push notifications, prompting them to accept push notifications
    * on their current device
    * @returns {void}
    */
  subscribeToNotifications(): void {
    const subModel = { serverPublicKey: this.VAPID_PUBLIC_KEY };
    this.swPush.requestSubscription(subModel)
      .then(sub => this.pushSubSvc.addPushSubscriber(sub))
      .catch(err => console.error("Could not subscribe to notifications", err));
    this.checkForPushUpdate();
  }

  /**
    * @description
    * When the user reloads the application
    * the Angular Service Worker will also do a call to the server to see if there is a 
    * new ngsw.json, and trigger the loading of any new files mentioned in the ngsw.json 
    * file in the background. When the observable emits, we confirm with the user, and then
    * load the newly deployed application.
    * @returns {void}
    */
  checkForPushUpdate(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm("New version available. Load New Version?")) {
          window.location.reload();
        }
      });
    }
  }
}
