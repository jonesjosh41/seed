import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

export const ROUTES: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'user', loadChildren: '../user/user.module#UserModule'},
  { path: 'events', loadChildren: '../scheduling/scheduling.module#SchedulingModule'},
  { path: 'notifications', loadChildren: '../notifications/notifications.module#NotificationModule' }
];


