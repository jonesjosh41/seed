export * from './create/create.component';
export * from './day/day.component';
export * from './month/month.component';
export * from './week/week.component';

// components
import { CreateComponent } from './create/create.component';
import { DayComponent } from './day/day.component';
import { MonthComponent } from './month/month.component';
import { WeekComponent } from './week/week.component';

// modals
import {
    CreateEventModalComponent,
    DayModalComponent,
    EventModalComponent,
    EditModalComponent,
    CreateCalendarModalComponent
} from './modals';

export const components = [
    CreateComponent,
    DayComponent,
    MonthComponent,
    WeekComponent,
    CreateEventModalComponent,
    CreateCalendarModalComponent,
    DayModalComponent,
    EventModalComponent,
    EditModalComponent
];

