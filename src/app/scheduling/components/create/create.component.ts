import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';

import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';
import * as Model from '@scheduling/models';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';

@Component({
  selector: 'app-event-create',
  template: `
    <div fxLayout="column" fxLayoutAlign="center start" fxLayoutGap="10px" *ngIf="clearance >= 1">
          <mat-card-header>
              <mat-card-title><strong><h1>Create An Event</h1></strong></mat-card-title>
              <mat-card-subtitle>create an event</mat-card-subtitle>
          </mat-card-header>

          <mat-divider></mat-divider>

          <mat-card-content>
              <app-form-group [config]="config" (submit)="formChanged($event)"></app-form-group>
          </mat-card-content>

          <mat-card-actions fxLayout>
            <button mat-raised-button color="primary" [disabled]="!valid" (click)="createEvent()">
              Create Event
            </button>
          </mat-card-actions>
    </div>
    <div *ngIf="clearance === 0">You must create an account in order to create an event</div>
  `,
  styles: [`
  `]
})
export class CreateComponent implements OnInit {
  @Output() newEvent = new EventEmitter<any>();


  @Input() clearance: number;
  @Input() keys: string[];
  @Input() calendars: Model.CalendarData[];

  config:  FieldConfig[];
  form: any;
  valid = false;

  calendarField = Field.eventCalendarField;

  constructor() { }

  ngOnInit() {

    Field.eventCalendarField.selectOptions = ['Public', 'Private', 'Company Events'];
    Field.eventStartDateField.value = new Date(`${2018}-${11}-${1}`).toLocaleDateString();
    Field.eventEndDateField.value = new Date(`${2018}-${11}-${1}`).toLocaleDateString();

    // this.calendarField.selectOptions = this.calendars.map(calendar => calendar.data.name);
    this.calendarField.selectOptions = ['Public', 'Private'];
    this.config = [
      Field.eventNameField,
      Field.eventStartDateField,
      Field.eventStartTimeField,
      Field.eventEndDateField,
      Field.eventEndTimeField,
      Field.eventDescriptionField,
      Field.color,
      this.calendarField,
    ];
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  createEvent() {
    const event = this.form.value;

    const year = event.startDate.getFullYear();
    const month = event.startDate.getMonth();
    const day = event.startDate.getDate(month, year, 0);

    const data: Model.EventData = {
      id: null, // this.db.createId(),
      date: {
        day: day,
        month: month,
        year: year,
      },
      owner: null,
      name: event.name,
      startTime: event.startTime,
      endTime: event.endTime,
      description: event.description,
      attending: null, // event.attending,
      done: false,
      calendar: event.calendar,
      color: event.color,
    };

    this.newEvent.emit(data);
  }
}

