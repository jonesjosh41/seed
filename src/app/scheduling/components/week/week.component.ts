import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';

import { Observable, fromEvent, of } from 'rxjs';
import { map, delay, distinct, distinctUntilChanged, filter, tap } from 'rxjs/operators';

import * as Model from '@scheduling/models';

import { timeOptions } from '@scheduling/models/forms';

@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeekComponent implements OnInit {
  @Input() events: Model.EventData[] = [];
  @Input() calendars: Model.CalendarData[];
  @Input() tiles: any;
  @Input() year: number;
  @Input() month: number;
  @Input() week: number;

  @Output() chosenDay = new EventEmitter<string>();
  @Output() chosenEvent = new EventEmitter<string>();

  day;
  startOfWeek;
  endOfWeek;
  days;
  dayTiles: any[];
  timeTiles: any;

  times = ['12', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
  mobileTimes = ['12', '3', '6', '9', '11'];
  dayAbbreviations = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  mobileDayAbbreviations = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

  constructor() { }

  ngOnInit() {

    this.startOfWeek = new Date(`11/1/1`);
    this.endOfWeek = new Date('11/1/8');
    this.days = this.generateWeekOfTiles();
  }

  onChooseDay(day) {
    this.chosenDay.emit(day);
  }
  onChooseEvent(event) {
    this.chosenEvent.emit(event);
  }


  mouseDown(event) {
    console.log(event);
  }
  mouseUp(event) {
    console.log(event);
  }
  mouseOver(event) {
    console.log(event);
  }
  addClass(event) {
    console.log(event);
  }

  generateWeekOfTiles() {
    const days = [];
    let tileConfig;
    for (let i = 0; i < 7; i++) {
        tileConfig = {
          day: this.day,
          week: this.week,
          year: this.year,
          tiles: []
        };

        days.push(tileConfig);
    }

    this.times.forEach(time => {
      days.map(day => {
        const color = '';
        const events = this.events.filter(event => (event.date.day === day.day)
                                                && (event.date.month === day.month)
                                                && (event.date.year === day.year)
                                                && (event.date.time === time));
        const css = '';

        const timeOfDay = {
          time: time,
          color: color,
          events: events,
          css: css
        };

        day.tiles.push(timeOfDay);
      });
    });

    console.log(days);

    return days;
  }


  createTileConfig(time, color, events, css): TimeOfDay {
    return {
      time: time,
      color: color,
      events: events,
      css: css
    };
  }

}

interface DayOfWeek {
  day: string;
  month: string;
  year: string;
  tiles: TimeOfDay[];
}

interface TimeOfDay {
  time: number;
  color: string;
  events: any[];
  css: string;
}
