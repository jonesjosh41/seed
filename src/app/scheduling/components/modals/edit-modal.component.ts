import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnChanges,
    SimpleChanges,
    Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as Field from '@scheduling/models/forms';
import * as Model from '@scheduling/models';

@Component({
    selector: 'app-edit-modal',
    template: ``,
    styles: [``],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditModalComponent implements OnInit, OnChanges {

constructor(
    dialogRef: MatDialogRef<EditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

ngOnInit() {}
ngOnChanges(changes: SimpleChanges) {}

}
