import { Component, OnInit, Inject } from '@angular/core';

import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';
import * as Model from '@scheduling/models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-create-calendar-modal',
  template: `
    <div fxLayout="column" fxLayoutAlign="center start" fxLayoutGap="10px" *ngIf="clearance >= 0">

        <h1 mat-dialog-title>Add a Calendar</h1>
        <app-form-group mat-dialog-content [config]="data.formConfig" (submit)="formChanged($event)"></app-form-group>

        <div mat-dialog-actions>
            <button mat-raised-button color="primary" [disabled]="!valid" (click)="submit()">
              Create Calendar
            </button>
        </div>
    </div>
    <div *ngIf="clearance === 0">You must create an account in order to create an calendar</div>
  `,
  styles: [`
  `]
})
export class CreateCalendarModalComponent implements OnInit {
  clearance = 1;
  config:  FieldConfig[];
  form: FormGroup;
  valid = false;

  constructor(
    private dialogRef: MatDialogRef<CreateCalendarModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.config) {
        this.config = [
            ...this.data.formConfig
          ];
    }
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.get('name').value;
  }

  submit() {
    this.dialogRef.close(this.form.value);
  }
}
