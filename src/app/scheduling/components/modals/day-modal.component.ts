import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnChanges,
    SimpleChanges,
    Inject
  } from '@angular/core';
  import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

  import * as Model from '@scheduling/models';

  @Component({
    selector: 'app-day-modal',
    template: `
        <div class="component-header">
            <h1>{{ title }}</h1>
        </div>
    `,
    styles: [``],
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  export class DayModalComponent implements OnInit, OnChanges {
    @Input() tiles: Model.TileConfig[];
    @Output() chosenDay = new EventEmitter<string>();
    @Output() chosenEvent = new EventEmitter<string>();

    title: any;

    constructor(
      dialogRef: MatDialogRef<DayModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
      console.log(this.data);
      this.title = new Date(this.data.year, this.data.month, this.data.day).toDateString();
    }
    ngOnChanges(changes: SimpleChanges) {}


    onChooseDay(day) {
      this.chosenDay.emit(day);
    }

    onChooseEvent(event) {
      this.chosenEvent.emit(event);
    }

  }
