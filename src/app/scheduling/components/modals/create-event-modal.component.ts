import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';

import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';
import * as Model from '@scheduling/models';
import { MAT_BOTTOM_SHEET_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-create-event-modal',
  template: `
    <div fxLayout="column" fxLayoutAlign="center start" fxLayoutGap="10px" *ngIf="clearance >= 0">
          <mat-card-title>
              <strong><h1>Create An Event</h1></strong>
              <mat-card-subtitle>create an event</mat-card-subtitle>
          </mat-card-title>

          <mat-divider></mat-divider>

          <mat-card-content>
              <app-form-group [config]="config" (submit)="formChanged($event)"></app-form-group>
          </mat-card-content>

          <mat-card-actions fxLayout>
            <button mat-raised-button color="primary" [disabled]="!valid" (click)="submit()">
              Create Calendar
            </button>
          </mat-card-actions>
    </div>
    <div *ngIf="clearance === 0">You must create an account in order to create an event</div>
  `,
  styles: [`
  `]
})
export class CreateEventModalComponent implements OnInit {
  @Output() newEvent = new EventEmitter<any>();
  clearance: number;
  config:  FieldConfig[];
  form: any;
  valid = false;

  constructor(
    private dialogRef: MatDialogRef<CreateEventModalComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
  ) { }

  ngOnInit() {

    this.config = [
      ...this.data.formConfig
    ];
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  submit() {
    this.dialogRef.close(this.form.value);
  }
}
