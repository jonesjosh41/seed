import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  Inject
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatBottomSheet } from '@angular/material';

import * as Model from '@scheduling/models';
import { CreateComponent } from '../create/create.component';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayComponent implements OnInit, OnChanges {
  @Input() tiles: Model.TileConfig[];
  @Output() chosenDay = new EventEmitter<string>();
  @Output() chosenEvent = new EventEmitter<string>();

  title: string;
  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {}


  onChooseDay(day) {
    this.chosenDay.emit(day);
  }

  onChooseEvent(event) {
    this.chosenEvent.emit(event);
  }

}
