import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges
} from '@angular/core';

import * as Model from '@scheduling/models';

@Component({
  selector: 'app-month',
  templateUrl: './month.component.html',
  styleUrls: ['./month.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonthComponent implements OnChanges {
  @Input() events: Model.EventData[];
  @Input() calendars: Model.CalendarData[];
  @Input() year: number;
  @Input() month: number;

  @Output() chosenEvent = new EventEmitter<Event>();
  @Output() chosenDay = new EventEmitter<any>();

  tiles: Model.TileConfig[];
  dayAbbreviations = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  mobileDayAbbreviations = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    this.tiles = this.generateMonthOfTiles(this.month, this.year, this.events, this.calendars);
  }

  onChooseEvent(event) {
    this.chosenEvent.emit(event);
  }

  onChooseDay(day) {
    this.chosenDay.emit(day);
  }

  generateMonthOfTiles(month: number, year: number, events: Model.EventData[], calendars: Model.CalendarData[]) {
    const tiles = [];
    const daysInCurrentMonth = new Date(year, month + 1, 0).getDate();
    const daysInPreviousMonth = new Date(year, month, 0).getDate();
    const firstDayOfCurrentMonth = new Date(`${year}-${month + 1}-01`).getDay();

    // Add days from previous calendar on the first row until the first day of the current month is reached
    if (firstDayOfCurrentMonth !== 6) {
        let dayFromPreviousMonth = daysInPreviousMonth - firstDayOfCurrentMonth;
        for (let i = 1; i <= firstDayOfCurrentMonth + 1; i++) {
            const tileConfig = this.createTileConfig(dayFromPreviousMonth, month - 1, year, '#F5F5F5', []);
            this.addEventsToDayTile(tileConfig, events, calendars);
            tiles.push(tileConfig);
            dayFromPreviousMonth++;
        }
    }

    // create a tile for every day of the current month
    for (let i = 1; i <= daysInCurrentMonth; i++) {
        const tileConfig = this.createTileConfig(i, month, year, 'white', []);

        this.addEventsToDayTile(tileConfig, events, calendars);

        tiles.push(tileConfig);
    }

    // add days from the next month until the viewport is filled
    if (tiles.length > 35) { // if it's over 35 tiles, anther row must be generated
        let i = 1;
        while (tiles.length !== 42) {
            const tileConfig = this.createTileConfig(i, month + 1, year, '#F5F5F5;', []);
            this.addEventsToDayTile(tileConfig, events, calendars);
            tiles.push(tileConfig);
            i++;
        }
    } else {
        let i = 1;
        while (tiles.length !== 35) {
            const tileConfig = this.createTileConfig(i, month + 1, year, 'F5F5F5', []);
            this.addEventsToDayTile(tileConfig, events, calendars);
            tiles.push(tileConfig);
            i++;
        }
    }

    return tiles;
}

addEventsToDayTile(tileConfig: Model.TileConfig, events: Model.EventData[], calendars: Model.CalendarData[]) {
    if (!!events && !!events.length && !events.includes(undefined)) {
      events.forEach(event => {
        if (event.date.day === tileConfig.day
              && event.date.month === tileConfig.month
              && event.date.year === tileConfig.year) {

          calendars.forEach(calendar => {
            if (calendar.name === event.calendar.name) {
              // event.data.color = calendar.color;
              console.log('event color:', event.color);
              console.log('calendar color:', calendar.color);
            }
          });

          tileConfig.events.push(event);
        }
      });
    }
}

createTileConfig(day, month, year, color, events) {
    return {
      day: day,
      month: month,
      year: year,
      color: color,
      events: events,
    };
}

  get calendarsShowing(): string[] {
    return this.calendars && this.calendars.length
          ? this.calendars
                  .filter(calendar => !!calendar.viewing)
                  .map(calendar => calendar.name)
                  : [];
  }
}
