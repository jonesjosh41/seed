import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { EventEntity } from '@scheduling/models/event.model';

import * as Model from '@scheduling/models';

// Actions that Query/Update the Database via Effect
export const GET_EVENT = '[Event Effect] | DB Get | Get Event';
export const GET_EVENTS = '[Event Effect] | DB Get | Get Set of Events';
export const CREATE_EVENT = '[Event Effect] | DB Set | Create Event';
export const UPDATE_EVENT = '[Event Effect] | DB Update | Update Event';
export const DELETE_EVENT = '[Event Effect] | DB Remove | Delete Event';

export class GetEvent implements Action {
  readonly type = GET_EVENT;
  constructor(public payload: string) {}
}

export class GetEvents implements Action {
  readonly type = GET_EVENTS;
  constructor(public payload: string) {}
}

export class CreateEvent implements Action {
  readonly type = CREATE_EVENT;
  constructor(public payload: Model.EventData) {}
}

export class UpdateEvent implements Action {
  readonly type = UPDATE_EVENT;
  constructor(public payload: Model.EventData) {}
}

export class DeleteEvent implements Action {
  readonly type = DELETE_EVENT;
  constructor(public payload: string) {}
}


// Actions That Update The State via Reducer
export enum EventActionTypes {
  AddEventEntity = '[Event Reducer] | Entity | Add Event',
  UpsertEventEntity = '[Event Reducer] | Entity | Upsert Event',
  AddEventEntities = '[Event Reducer] | Entity | Add Events',
  UpsertEventEntities = '[Event Reducer] | Entity | Upsert Events',
  UpdateEventEntity = '[Event Reducer] | Entity | Update Event',
  UpdateEventEntities = '[Event Reducer] | Entity | Update Events',
  DeleteEventEntity = '[Event Reducer] | Entity | Delete Event',
  DeleteEventEntities = '[Event Reducer] | Entity | Delete Events',
  ClearEventEntities = '[Event Reducer] | Entity | Clear Events'
}

export class AddEventEntity implements Action {
  readonly type = EventActionTypes.AddEventEntity;
  constructor(public payload: { event: EventEntity }) {}
}

export class UpdateEventEntity implements Action {
  readonly type = EventActionTypes.UpdateEventEntity;
  constructor(public payload: { event: Update<EventEntity> }) {}
}

export class UpsertEventEntity implements Action {
  readonly type = EventActionTypes.UpsertEventEntity;
  constructor(public payload: { event: EventEntity }) {}
}

export class AddEventEntities implements Action {
  readonly type = EventActionTypes.AddEventEntities;
  constructor(public payload: { events: EventEntity[] }) {}
}

export class UpsertEventEntities implements Action {
  readonly type = EventActionTypes.UpsertEventEntities;
  constructor(public payload: { events: EventEntity[] }) {}
}

export class UpdateEventEntities implements Action {
  readonly type = EventActionTypes.UpdateEventEntities;
  constructor(public payload: { events: Update<EventEntity>[] }) {}
}

export class DeleteEventEntity implements Action {
  readonly type = EventActionTypes.DeleteEventEntity;
  constructor(public payload: { id: string }) {}
}

export class DeleteEventEntities implements Action {
  readonly type = EventActionTypes.DeleteEventEntities;
  constructor(public payload: { ids: string[] }) {}
}

export class ClearEventEntities implements Action {
  readonly type = EventActionTypes.ClearEventEntities;
}

export type EventActions = AddEventEntity
 | UpdateEventEntity
 | UpsertEventEntity
 | DeleteEventEntity
 | AddEventEntities
 | UpsertEventEntities
 | UpdateEventEntities
 | DeleteEventEntities
 | ClearEventEntities
 | CreateEvent
 | GetEvent
 | GetEvents
 | DeleteEvent
 | UpdateEvent;
