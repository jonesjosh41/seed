import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { CalendarEntity } from '@scheduling/models/calendars.model';

import * as Model from '@scheduling/models';

// Actions that Query/Update the Database via Effect
export const GET_CALENDAR = '[Calendar Effect] | DB Get | Get Calendar';
export const GET_CALENDARS = '[Calendar Effect] | DB Get | Get Set of Calendars';
export const GET_EMPLOYEE_CALENDARS = '[Calendar Effect] | DB Get | Get Employee Calendars';
export const GET_CUSTOMER_CALENDARS = '[Calendar Effect] | DB Get | Get Customer Calendars';
export const CREATE_CALENDAR = '[Calendar Effect] | DB Set | Create Calendar';
export const UPDATE_CALENDAR = '[Calendar Effect] | DB Update | Update Calendar';
export const DELETE_CALENDAR = '[Calendar Effect] | DB Remove | Delete Calendar';


export class GetCalendar implements Action {
  readonly type = GET_CALENDAR;
  constructor(public payload: string) {}
}

export class GetCalendars implements Action {
  readonly type = GET_CALENDARS;
  constructor(public payload: string) {}
}

export class CreateCalendar implements Action {
  readonly type = CREATE_CALENDAR;
  constructor(public payload: Model.CalendarData) {}
}

export class UpdateCalendar implements Action {
  readonly type = UPDATE_CALENDAR;
  constructor(public payload: Model.CalendarData) {}
}

export class DeleteCalendar implements Action {
  readonly type = DELETE_CALENDAR;
  constructor(public payload: string) {}
}


// Actions That Update The State via Reducer
export enum CalendarActionTypes {
  AddCalendarEntity = '[Calendar Reducer] | Entity | Add Calendar',
  UpsertCalendarEntity = '[Calendar Reducer] | Entity | Upsert Calendar',
  AddCalendarEntities = '[Calendar Reducer] | Entity | Add Calendars',
  UpsertCalendarEntities = '[Calendar Reducer] | Entity | Upsert Calendars',
  UpdateCalendarEntity = '[Calendar Reducer] | Entity | Update Calendar',
  UpdateCalendarEntities = '[Calendar Reducer] | Entity | Update Calendars',
  DeleteCalendarEntity = '[Calendar Reducer] | Entity | Delete Calendar',
  DeleteCalendarEntities = '[Calendar Reducer] | Entity | Delete Calendars',
  ClearCalendarEntities = '[Calendar Reducer] | Entity | Clear Calendars'
}

export class AddCalendarEntity implements Action {
  readonly type = CalendarActionTypes.AddCalendarEntity;
  constructor(public payload: { calendar: CalendarEntity }) {}
}

export class UpdateCalendarEntity implements Action {
  readonly type = CalendarActionTypes.UpdateCalendarEntity;
  constructor(public payload: { calendar: Update<CalendarEntity> }) {}
}

export class UpsertCalendarEntity implements Action {
  readonly type = CalendarActionTypes.UpsertCalendarEntity;
  constructor(public payload: { calendar: CalendarEntity }) {}
}

export class AddCalendarEntities implements Action {
  readonly type = CalendarActionTypes.AddCalendarEntities;
  constructor(public payload: { calendars: CalendarEntity[] }) {}
}

export class UpsertCalendarEntities implements Action {
  readonly type = CalendarActionTypes.UpsertCalendarEntities;
  constructor(public payload: { calendars: CalendarEntity[] }) {}
}

export class UpdateCalendarEntities implements Action {
  readonly type = CalendarActionTypes.UpdateCalendarEntities;
  constructor(public payload: { calendars: Update<CalendarEntity>[] }) {}
}

export class DeleteCalendarEntity implements Action {
  readonly type = CalendarActionTypes.DeleteCalendarEntity;
  constructor(public payload: { id: string }) {}
}

export class DeleteCalendarEntities implements Action {
  readonly type = CalendarActionTypes.DeleteCalendarEntities;
  constructor(public payload: { ids: string[] }) {}
}

export class ClearCalendarEntities implements Action {
  readonly type = CalendarActionTypes.ClearCalendarEntities;
}

export type CalendarActions = AddCalendarEntity
 | UpdateCalendarEntity
 | UpsertCalendarEntity
 | DeleteCalendarEntity
 | AddCalendarEntities
 | UpsertCalendarEntities
 | UpdateCalendarEntities
 | DeleteCalendarEntities
 | ClearCalendarEntities
 | CreateCalendar
 | GetCalendar
 | GetCalendars
 | DeleteCalendar
 | UpdateCalendar;
