import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';

/* NGRX */
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';

/* RXJS */
import { take, zip, switchMap, catchError, map, pluck, tap, concatMap, withLatestFrom, delay } from 'rxjs/operators';
import { forkJoin, of, from } from 'rxjs';

/* Store */
import * as CoreAction from '@core/store/actions';
import * as Action from '@scheduling/store/actions';
import * as Model from '../../models';
import * as UserSelector from '@user/store/selectors';


@Injectable()
export class CalendarEffects {

  constructor(
    private actions$: Actions,
    private db: AngularFirestore,
    private store: Store<any>,
  ) {}

  @Effect()
  getSingle$ = this.actions$
    .ofType(Action.GET_CALENDAR)
    .pipe(
        map((action: Action.GetCalendar): string => action.payload),
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        switchMap(([calendarId, auth]) => this.db
            .collection('users') // parent collection
            .doc(auth.role) // role based database structure
            .collection('calendars') // parent collection
            .doc(auth.uid) // this users document
            .collection('calendars') // this users calendar sub collection
            .doc<Model.CalendarData>(calendarId) // the id of the calendar we want
            .valueChanges()
        ),
        map((calendar) => new Action.AddCalendarEntity({ calendar: { id: calendar.id, data: { ...calendar }}})),
        catchError(err => of(console.log(err)))
    );

  @Effect({ dispatch: false })
  create$ = this.actions$
      .ofType(Action.CREATE_CALENDAR)
      .pipe(
        map((action: Action.CreateCalendar): Model.CalendarData => action.payload),
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        // use auth data to navigate to specified sub collection, set new document
        tap(([calendar, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('calendars')
            .doc(auth.uid)
            .collection('calendars')
            .doc<Model.CalendarData>(calendar.id)
            .set(calendar)
        ),
      );

  @Effect()
  getList$ = this.actions$
    .ofType(Action.GET_CALENDARS)
    .pipe(
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        // Get a set of calendars with the role given in the payload
        switchMap(([role, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('calendars')
            .doc(auth.uid)
            .collection('calendars')
            .valueChanges()
        ),
        // Transform the set of calendars into a set of Entities
        map((calendarDataArray: Model.CalendarData[]) => calendarDataArray.map((calendar) => {
          return { id: calendar.id, data: { ...calendar } };
        })),
        // Add set of Entities to the calendar state
        map((calendarEntityArray: Model.CalendarEntity[]) => new Action.AddCalendarEntities( { calendars: calendarEntityArray })),
        catchError(err => of(console.log(err)))
    );

  @Effect()
  update$ = this.actions$
    .ofType(Action.UPDATE_CALENDAR)
    .pipe(
      map((action: Action.UpdateCalendar): Model.CalendarData => action.payload),
      // get this users uid and role from state
      withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
      // Update the Firestore Database
      tap(([calendar, auth]) => this.db
        .collection('users')
        .doc(auth.role)
        .collection('calendars')
        .doc<Model.CalendarData>(auth.uid)
        .collection('calendars')
        .doc(calendar.id)
        .update(calendar)
      ),
      // Grab Data from Firestore Database
      switchMap(([calendar, auth]) => this.db
        .collection('users')
        .doc(auth.role)
        .collection<Model.CalendarData>('calendars')
        .doc<Model.CalendarData>(auth.uid)
        .collection('calendars')
        .doc(calendar.id)
        .valueChanges()
      ),
      concatMap((calendar: Model.CalendarData) => [
        // Set the Application state in the reducer, payload is in an Entity format
        new Action.UpsertCalendarEntity({ calendar: { id: calendar.id, data: { ...calendar }}}),
        // Give the User Feedback assuming the db set was successful
        new CoreAction.ShowSnackbar('Your Calendar Has Been Updated')
      ]),
      catchError((err) => forkJoin(() => [
        // Log the Error
        console.log(err),
        // Give user feedback on the error
        new CoreAction.ShowSnackbar(err.message)
      ]))
    );

  @Effect()
  delete$ = this.actions$
    .ofType( Action.CalendarActionTypes.DeleteCalendarEntity)
    .pipe(
        map((action: Action.DeleteCalendar): string => action.payload),
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        tap(([calendarId, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('calendars')
            .doc(auth.uid)
            .collection('calendars')
            .doc<Model.CalendarData>(calendarId)
            .delete()
        ),
        map(([calendarId, auth]) => new Action.DeleteCalendarEntity({ id: calendarId }))
    );
}
