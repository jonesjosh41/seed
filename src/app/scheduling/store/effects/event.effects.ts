import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';

/* NGRX */
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';

/* RXJS */
import { take, zip, switchMap, catchError, map, pluck, tap, concatMap, withLatestFrom, delay } from 'rxjs/operators';
import { forkJoin, of, from } from 'rxjs';

/* Store */
import * as CoreAction from '@core/store/actions';
import * as Action from '@scheduling/store/actions';
import * as Model from '../../models';
import * as UserSelector from '@user/store/selectors';


@Injectable()
export class EventEffects {

  constructor(
    private actions$: Actions,
    private db: AngularFirestore,
    private store: Store<any>,
  ) {}

  @Effect()
  getSingle$ = this.actions$
    .ofType(Action.GET_EVENT)
    .pipe(
        map((action: Action.GetEvent): string => action.payload),
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        switchMap(([eventId, auth]) => this.db
            .collection('users') // parent collection
            .doc(auth.role) // role based database structure
            .collection('events') // parent collection
            .doc(auth.uid) // this users document
            .collection('events') // this users event sub collection
            .doc<Model.EventData>(eventId) // the id of the event we want
            .valueChanges()
        ),
        map((event) => new Action.AddEventEntity({ event: { id: event.id, data: { ...event }}})),
        catchError(err => of(console.log(err)))
    );

  @Effect({ dispatch: false })
  create$ = this.actions$
      .ofType(Action.CREATE_EVENT)
      .pipe(
        map((action: Action.CreateEvent): Model.EventData => action.payload),
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        // use auth data to navigate to specified sub collection, set new document
        tap(([event, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('events')
            .doc(auth.uid)
            .collection('events')
            .doc<Model.EventData>(event.id)
            .set(event)
        ),
      );

  @Effect()
  getList$ = this.actions$
    .ofType(Action.GET_EVENTS)
    .pipe(
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        // Get a set of events with the role given in the payload
        switchMap(([role, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('events')
            .doc(auth.uid)
            .collection('events')
            .valueChanges()
        ),
        // Transform the set of events into a set of Entities
        map((eventDataArray: Model.EventData[]) => eventDataArray.map((event) => {
          return { id: event.id, data: { ...event } };
        })),
        // Add set of Entities to the event state
        map((eventEntityArray: Model.EventEntity[]) => new Action.AddEventEntities( { events: eventEntityArray })),
        catchError(err => of(console.log(err)))
    );

  @Effect()
  update$ = this.actions$
    .ofType(Action.UPDATE_EVENT)
    .pipe(
      map((action: Action.UpdateEvent): Model.EventData => action.payload),
      // get this users uid and role from state
      withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
      // Update the Firestore Database
      tap(([event, auth]) => this.db
        .collection('users')
        .doc(auth.role)
        .collection('events')
        .doc<Model.EventData>(auth.uid)
        .collection('events')
        .doc(event.id)
        .update(event)
      ),
      // Grab Data from Firestore Database
      switchMap(([event, auth]) => this.db
        .collection('users')
        .doc(auth.role)
        .collection<Model.EventData>('events')
        .doc<Model.EventData>(auth.uid)
        .collection('events')
        .doc(event.id)
        .valueChanges()
      ),
      concatMap((event: Model.EventData) => [
        // Set the Application state in the reducer, payload is in an Entity format
        new Action.UpsertEventEntity({ event: { id: event.id, data: { ...event }}}),
        // Give the User Feedback assuming the db set was successful
        new CoreAction.ShowSnackbar('Your Event Has Been Updated')
      ]),
      catchError((err) => forkJoin(() => [
        // Log the Error
        console.log(err),
        // Give user feedback on the error
        new CoreAction.ShowSnackbar(err.message)
      ]))
    );

  @Effect()
  delete$ = this.actions$
    .ofType( Action.EventActionTypes.DeleteEventEntity)
    .pipe(
        map((action: Action.DeleteEvent): string => action.payload),
        // get this users uid and role from state
        withLatestFrom(this.store.pipe(select(UserSelector.getUidAndRole))),
        tap(([eventId, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('events')
            .doc(auth.uid)
            .collection('events')
            .doc<Model.EventData>(eventId)
            .delete()
        ),
        map(([eventId, auth]) => new Action.DeleteEventEntity({ id: eventId }))
    );
}
