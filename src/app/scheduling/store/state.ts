import * as schedulingEffect from '@scheduling/store/effects';

import * as SchedulingReducer from '@scheduling/store/reducers';

import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

// Model
export interface SchedulingState {
    event: SchedulingReducer.EventState;
    calendar: SchedulingReducer.CalendarState;
}

// Reducer
export const reducers: ActionReducerMap<SchedulingState> = {
    event: SchedulingReducer.eventReducer,
    calendar: SchedulingReducer.calendarReducer,
};

// Effects
export const effects: any[] = [
    schedulingEffect.EventEffects,
    schedulingEffect.CalendarEffects,
];

// Selector
export const getSchedulingState = createFeatureSelector<SchedulingState>('scheduling');

