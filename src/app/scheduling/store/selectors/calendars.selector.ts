/*
DATA TO GET:
1:  Array of Users
2:  Specific User By uid
*/

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as CalendarReducer from '@scheduling/store/reducers/calendar.reducer';
import * as Model from '@scheduling/models';

import { getCurrentUid } from '@user/store/selectors/auth.selector';
import { getSchedulingState } from '@scheduling/store/state';
import { getRouterParamId } from '@core/store/selectors';

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = CalendarReducer.calendarAdapter.getSelectors();

export const getCalendarState = createSelector(
  getSchedulingState,
  (state) => state.calendar
);

export const getArrayOfCalendars = createSelector(
  getCalendarState,
  (calendarState): Model.CalendarData[] => {
    const entities = calendarState.entities;
    return Object.keys(entities)
      .map(id => entities[id])
      .map(calendar => calendar.data);
  }
);

export const getCalendarEntities = createSelector(
  getCalendarState,
  (state) =>  state.entities
);

export const getSelectedCalendar = createSelector(
    getCalendarState,
    (state) => state // state.selected
);

export const getSelectedCalendarFromRouteParams = createSelector(
  getRouterParamId,
  selectEntities,
  (id: number, entities): Model.CalendarEntity => entities[id]
);

