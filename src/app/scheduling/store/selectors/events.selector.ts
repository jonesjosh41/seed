/*
DATA TO GET:
1:  Array of Users
2:  Specific User By uid
*/

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as EventReducer from '@scheduling/store/reducers/event.reducer';
import * as Model from '@scheduling/models';

import { getCurrentUid } from '@user/store/selectors/auth.selector';
import { getSchedulingState } from '@scheduling/store/state';
import { getRouterParamId } from '@core/store/selectors';

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = EventReducer.eventAdapter.getSelectors();

export const getEventState = createSelector(
  getSchedulingState,
  (state) => state.event
);

export const getArrayOfEvents = createSelector(
  getEventState,
  (eventState): Model.EventData[] => {
    const entities = eventState.entities;
    return Object.keys(entities)
      .map(id => entities[id])
      .map(event => event.data);
  }
);

export const getEventEntities = createSelector(
  getEventState,
  (state) =>  state.entities
);

export const getSelectedEvent = createSelector(
    getEventState,
    (state) => state // state.selected
);

export const getSelectedEventFromRouteParams = createSelector(
  getRouterParamId,
  selectEntities,
  (id: number, entities): Model.EventData => entities[id].data
);

