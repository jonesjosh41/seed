import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import * as Model from '@scheduling/models';
import { CalendarActions, CalendarActionTypes } from '../actions/calendar.actions';

export interface CalendarState extends EntityState<Model.CalendarEntity> {
  // additional entities state properties
}

export const calendarAdapter: EntityAdapter<Model.CalendarEntity> = createEntityAdapter<Model.CalendarEntity>();

const initialState: CalendarState = calendarAdapter.getInitialState({
  // additional entity state properties
});

export function calendarReducer(
  state = initialState,
  action: CalendarActions
): CalendarState {
  switch (action.type) {

    case CalendarActionTypes.AddCalendarEntity: {
      return calendarAdapter.addOne(action.payload.calendar, state);
    }

    case CalendarActionTypes.UpsertCalendarEntity: {
      return calendarAdapter.upsertOne(action.payload.calendar, state);
    }

    case CalendarActionTypes.AddCalendarEntities: {
      return calendarAdapter.addMany(action.payload.calendars, state);
    }

    case CalendarActionTypes.UpsertCalendarEntities: {
      return calendarAdapter.upsertMany(action.payload.calendars, state);
    }

    case CalendarActionTypes.UpdateCalendarEntity: {
      return calendarAdapter.updateOne(action.payload.calendar, state);
    }

    case CalendarActionTypes.UpdateCalendarEntities: {
      return calendarAdapter.updateMany(action.payload.calendars, state);
    }

    case CalendarActionTypes.DeleteCalendarEntity: {
      return calendarAdapter.removeOne(action.payload.id, state);
    }

    case CalendarActionTypes.DeleteCalendarEntities: {
      return calendarAdapter.removeMany(action.payload.ids, state);
    }

    case CalendarActionTypes.ClearCalendarEntities: {
      return calendarAdapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}
