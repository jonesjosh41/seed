import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import * as Model from '@scheduling/models';
import { EventActions, EventActionTypes } from '../actions/event.actions';

export interface EventState extends EntityState<Model.EventEntity> {
  // additional entities state properties
}

export const eventAdapter: EntityAdapter<Model.EventEntity> = createEntityAdapter<Model.EventEntity>();

const initialState: EventState = eventAdapter.getInitialState({
  // additional entity state properties
});

export function eventReducer(
  state = initialState,
  action: EventActions
): EventState {
  switch (action.type) {

    case EventActionTypes.AddEventEntity: {
      return eventAdapter.addOne(action.payload.event, state);
    }

    case EventActionTypes.UpsertEventEntity: {
      return eventAdapter.upsertOne(action.payload.event, state);
    }

    case EventActionTypes.AddEventEntities: {
      return eventAdapter.addMany(action.payload.events, state);
    }

    case EventActionTypes.UpsertEventEntities: {
      return eventAdapter.upsertMany(action.payload.events, state);
    }

    case EventActionTypes.UpdateEventEntity: {
      return eventAdapter.updateOne(action.payload.event, state);
    }

    case EventActionTypes.UpdateEventEntities: {
      return eventAdapter.updateMany(action.payload.events, state);
    }

    case EventActionTypes.DeleteEventEntity: {
      return eventAdapter.removeOne(action.payload.id, state);
    }

    case EventActionTypes.DeleteEventEntities: {
      return eventAdapter.removeMany(action.payload.ids, state);
    }

    case EventActionTypes.ClearEventEntities: {
      return eventAdapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}
