import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

/* NGRX */
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Components
import { components } from './components';

// Containers
import { containers } from './containers';

// Routes
import { ROUTES } from './containers/routes';

// Store
import { reducers, effects } from './store/state';

// Shared
import { SharedModule } from '@shared/shared.module';
import { GlobalButtonComponent } from '@shared/components/global-button/global-button.component';

// Forms
import { RBCFormsModule } from '@rbcForms/forms.module';
import { formFieldComponents } from '@rbcForms/components';

@NgModule({
  declarations: [
    ...components,
    ...containers,
  ],
  imports: [
    RouterModule.forChild(ROUTES),
    StoreModule.forFeature('scheduling', reducers),
    EffectsModule.forFeature(effects),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RBCFormsModule,
  ],
  entryComponents: [
    ...components,
    ...formFieldComponents,
    GlobalButtonComponent
  ]
})
export class SchedulingModule { }
