import { CalendarData } from './calendars.model';

export interface EventEffectPayload<T> {
  role?: string;
  data: T;
}

export interface EventEntity {
  id: string;
  data: EventData;
}

export interface EventData {
  id: string;
  date: EventDate;
  owner: string; // owner uid
  name: string;
  startTime: string;
  endTime: string;
  description: string;
  attending: string[]; /* list of uids */
  done: boolean;
  calendar: CalendarData;
  color: string;
}

export interface EventDate {
  day: number;
  month: number;
  year: number;
  time?: string;
}

export interface TileConfig {
  day: number;
  month: number;
  year: number;
  events: EventData[];
  color: string;
}
