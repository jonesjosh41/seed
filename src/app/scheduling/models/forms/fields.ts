import * as FormModel from '../../../forms/models';

export const timeOptions = [
    '12:00 AM',
    '1:00 AM',
    '2:00 AM',
    '3:00 AM',
    '4:00 AM',
    '5:00 AM',
    '6:00 AM',
    '7:00 AM',
    '8:00 AM',
    '9:00 AM',
    '10:00 AM',
    '11:00 AM',
    '12:00 PM',
    '1:00 PM',
    '2:00 PM',
    '3:00 PM',
    '4:00 PM',
    '5:00 PM',
    '6:00 PM',
    '7:00 PM',
    '8:00 PM',
    '9:00 PM',
    '10:00 PM',
    '11:00 PM',
];

export const dateField: FormModel.FieldConfig = {
    formControlName: 'date',
    componentType: 'date',
    value: '',
    label: 'choose month',
    inputConfig: {
        type: 'date',
    }
};

export const viewTypeField: FormModel.FieldConfig = {
    formControlName: 'currentView',
    componentType: 'select',
    value: 'Month',
    label: 'Current View',
    selectOptions: [
        'Month',
        'Week',
        'Day',
    ],
    displayConfig: {
        size: '100%',
        appearance: 'outline',
        marginTop: '5%',
    },
    extras: {
        help: 'choose the format to view events'
    }
};

export const eventNameField: FormModel.FieldConfig = {
    componentType: 'input',
    formControlName: 'name',
    label: 'Event Title',
    inputConfig: {
        type: 'text',
        required: true,
    }
};

export const eventStartDateField: FormModel.FieldConfig = {
    displayConfig: {
        size: '14%',
        marginRight: '0%'
    },
    componentType: 'date',
    formControlName: 'startDate',
    label: 'Start Date',
    value: '11/22/2018',
    inputConfig: {
        type: 'date',
        required: true,
    }
};

export const eventEndDateField: FormModel.FieldConfig = {
    displayConfig: {
        size: '14%',
        marginLeft: '0%',
        marginRight: '0%'
    },
    value: '11/22/2018',
    componentType: 'date',
    formControlName: 'endDate',
    label: 'End Date',
    inputConfig: {
        required: true
    }
};

export const eventStartTimeField: FormModel.FieldConfig = {
    displayConfig: {
        size: '14%',
        marginLeft: '0%',
        marginRight: '2%'
    },
    componentType: 'select',
    selectOptions: [ ...timeOptions],
    formControlName: 'startTime',
    label: 'time',
    inputConfig: {
        type: 'text',
        required: true,
    }
};

export const eventEndTimeField: FormModel.FieldConfig = {
    displayConfig: {
        size: '14%',
        marginLeft: '0%',
        marginRight: '0%'
    },
    componentType: 'select',
    selectOptions: [ ...timeOptions],
    formControlName: 'endTime',
    label: 'time',
    inputConfig: {
        required: true
    }

};

export const eventDescriptionField: FormModel.FieldConfig = {
    displayConfig: {
        appearance: 'outline',
        size: '100%'
    },
    componentType: 'textarea',
    formControlName: 'description',
    label: 'Event Description',
    inputConfig: {
        type: 'text',
        required: true,
    }
};

export const eventAttendeesField: FormModel.FieldConfig = {
    componentType: 'select',
    formControlName: 'attendees',
    selectOptions: [],
    label: 'Event Attendees',
    inputConfig: {
        required: true,
    }
};

export const eventCalendarsField: FormModel.FieldConfig = {
    componentType: 'select',
    formControlName: 'calendars',
    selectOptions: [],
    label: 'Add To Calendar',
    value: '',
    inputConfig: {
        required: true,
    }
};

export const eventColorField: FormModel.FieldConfig = {
    componentType: 'select',
    formControlName: 'color',
    selectOptions: [],
    label: 'Event Color',
    inputConfig: {
        required: true,
    }
};

export const eventCalendarField: FormModel.FieldConfig = {
    formControlName: 'calendar',
    componentType: 'select',
    selectOptions: [],
    label: 'Choose the Calendar you wish this event to be a part of',
    inputConfig: {
        required: true,
    }
};

export const name: FormModel.FieldConfig = {
    formControlName: 'name',
    componentType: 'input',
    label: 'Name',
    inputConfig: {
        required: true,
    }
};

export const color: FormModel.FieldConfig = {
    formControlName: 'color',
    componentType: 'color',
    value: '#eeeeee',
    label: 'Choose a Color'
};




