/* NOTE:
EVENTS are filtered by the CALENDAR they are associated with
*/

export interface CalendarEffectPayload<T> {
  role?: string;
  data: T;
}

export interface CalendarEntity {
  id: string;
  data: CalendarData;
}

export interface CalendarData {
  id: string;
  name: string;
  owner: string;
  color: string;
  viewing: boolean;
  keysRequired: string[];
}

