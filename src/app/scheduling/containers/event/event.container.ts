import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

import { FieldConfig } from '@rbcForms/models';
import * as Field from '@scheduling/models/forms';

import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';

import * as Action from '@scheduling/store/actions';
import * as Selector from '@scheduling/store/selectors';
import * as UserStore from '@user/store';
import * as CoreStore from '@core/store';
import { MatBottomSheetRef } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-event',
  template: `
    <button mat-raised-button (click)="onBack()">Go Back</button>
    <ng-container *ngIf="(pageVerb$ | async) === 'CREATE'">
      <app-event-create
        (newEvent)="createEvent($event)"
        [clearance]="clearance$ | async"
        [keys]="keys$ | async"
        [calendars]="calendars$ | async">
      </app-event-create>
    </ng-container>

    <ng-container *ngIf="(pageVerb$ | async) === 'UPDATE'">
    </ng-container>

    <ng-container *ngIf="(pageVerb$ | async) === 'READ'">
    </ng-container>
  `,
  styles: [`
  `]
})
export class EventComponent implements OnInit {
  pageVerb$: Observable<string>;
  calendars$: Observable<Model.CalendarData[]>;
  selectedEvent$: Observable<Model.EventData>;
  permissions$: Observable<UserModel.Permissions>;

  constructor(
    private store: Store<any>,
    private db: AngularFirestore
  ) { }

  ngOnInit() {
    this.pageVerb$ = this.store.select(CoreStore.getPageVerb);
    this.permissions$ = this.store.select(UserStore.getPermissionsState);
    this.calendars$ = this.store.select(Selector.getArrayOfCalendars);
    this.selectedEvent$ = this.store.select(Selector.getSelectedEventFromRouteParams);
  }

  createEvent(event) {
    console.log(event);
    // this.store.dispatch(new Action.CreateEvent(event));
  }

  updateEvent(event) {
    console.log(event);
    // this.store.dispatch(new Action.UpdateEvent(event));
  }

  deleteEvent(eventId) {
    console.log(eventId);
    // this.store.dispatch(new Action.DeleteEvent(eventId));
  }

  onBack() {
    this.store.dispatch(new CoreStore.Back());
  }

}

