export * from './calendar/calendar.container';
export * from './event/event.container';

import { CalendarComponent } from './calendar/calendar.container';
import { EventComponent } from './event/event.container';

export const containers = [
    CalendarComponent,
    EventComponent
];
