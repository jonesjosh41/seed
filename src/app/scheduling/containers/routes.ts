import { Routes } from '@angular/router';
import * as Container from './index';

export const ROUTES: Routes = [
    {
        path: 'calendar',
        component: Container.CalendarComponent
    },
    {
        path: 'event/:id',
        component: Container.EventComponent,
        data: {
            verb: 'READ'
        }
    },
    {
        path: 'create',
        component: Container.EventComponent,
        data: {
            verb: 'CREATE'
        }
    },
    {
        path: 'event/:id/update',
        component: Container.EventComponent,
        data: {
            verb: 'UPDATE'
        }
    }
];
