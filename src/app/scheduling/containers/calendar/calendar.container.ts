import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

// Seed Imports
import { FieldConfig } from '@rbcForms/models';
import { DayModalComponent, CreateEventModalComponent } from '@scheduling/components/modals';
import { CreateCalendarModalComponent } from '@scheduling/components/modals/create-calendar-modal.component';

// Store
import * as Selector from '@scheduling/store/selectors';
import * as Action from '@scheduling/store/actions';
import * as UserStore from '@user/store';
import * as CoreStore from '@core/store';

// Models
import * as Model from '@scheduling/models';
import * as UserModel from '@user/models';

// Forms
import * as Field from '@scheduling/models/forms';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.container.html',
  styleUrls: ['./calendar.container.scss']
})
export class CalendarComponent implements OnInit {
  permissions$: Observable<UserModel.Permissions>;
  events$: Observable<Model.EventData[]>;
  calendars$: Observable<Model.CalendarData[]>;

  month: number;
  day: number;
  year: number;
  title: string;

  toggled: boolean;
  currentView: 'Month' | 'Day' | 'Week';
  viewSelectForm: FieldConfig[] = [
    Field.viewTypeField
  ];

  constructor(
    private store: Store<any>,
    private db: AngularFirestore,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.month = new Date().getMonth();
    this.year = new Date().getFullYear();
    this.day = new Date().getDate();

    this.currentView = 'Month';
    this.setCurrentTitle(this.month);

    this.permissions$ = this.store.select(UserStore.getPermissionsState);
    this.events$ = this.store.select(Selector.getArrayOfEvents);
    this.calendars$ = this.store.select(Selector.getArrayOfCalendars);

/*     this.permissions$
      .pipe(delay(1000)) // make sure this users permissions have been returned
      .subscribe(auth => this.store.dispatch(new Action.GetEvents('customer'))); */

  }

  filterByCalendar(calendar) {
    console.log(calendar);
    let cal: Model.CalendarData;
    cal = {
      ...calendar
    };
    // this.store.dispatch(new Action.UpdateCalendarEntity({ calendar: cal }));
  }

  changeView(value) {
    this.currentView = value.get('currentView').value;
  }

  changeMonth(event) {
    this.month = event.getMonth();
    this.setCurrentTitle(this.month);
  }

  previousMonth() {
    if (this.month !== 0) {
      this.month--;
    } else {
      this.month = 11;
      this.year--;
    }
    this.setCurrentTitle(this.month);
  }

  nextMonth() {
    if (this.month !== 11) {
      this.month++;
    } else {
      this.month = 0;
      this.year++;
    }
    this.setCurrentTitle(this.month);
  }

  setCurrentTitle(month: number) {
    // tslint:disable-next-line:max-line-length
    const months = ['January', 'Feburary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.title = `${months[month]} ${this.year}`;
  }

  chooseDay(tileData: Model.TileConfig) {
    console.log(tileData);
    const dialogRef = this.dialog.open(DayModalComponent, {
      data: { ...tileData }
    });
  }

  addCalendar() {
    const dialogConfig = new MatDialogConfig();

    const form: FieldConfig[] = [
      Field.name,
      Field.color,
    ];

    dialogConfig.data = {
        title: 'Add a Calendar',
        formConfig: form
    };

    const dialogRef = this.dialog.open(CreateCalendarModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(data => {
      const calendar: Model.CalendarData = {
        id: this.db.createId(),
        name: data.name,
        owner: null, // this.currentUser,
        color: data.color,
        viewing: true,
        keysRequired: ['customer']
      };
      // this.store.dispatch(new Action.CreateUserCalendar(calendar));
    });
  }

  addEvent() {
      this.store.dispatch(new CoreStore.Go({
        path: ['/events/create']
      }));
  }

}
