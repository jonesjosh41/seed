# User Feature
**Description**:  The Sub modules in this feature are each things a specific user would interact with.  These sub modules are meant to be expanded based on the needs of the specific application and act as a general catch all for many common features. However, new sub modules can be added as needed.  Currently, there are 5 sub modules:
- auth:  authenticates users with firebase auth
- permissions:  controls what a specific user can or can't see
- chat:  allows a user to send messages to another user or group of users
- event:  allows a user to schedule / view / etc any type of event
- profile:  allows a user to view/edit information about themselves or other users

## Auth
The Auth sub feature uses the angularfire2 auth module to perform common operations involving authenticating a user.

### Components
- auth form:  the form used for logging in and registering new users

### Containers
- login:  the page that logs in already created users
- register:  the page that registers a new user

### Model
- Auth Model
 - uid:  the user id
 - emailVerified:  whether the user has a verified email address
 - exists:  whether the user exists in the database. used for initial state load and for guest users

### Actions
### Effects
### Reducers

## Permissions
The permissions sub feature allows you to display HTML elements dynamically as well as set up route guards based on a specific users permissions level.  The goal of permissions is to create an easy way for you to find out if a user can view something.  Specifically, it gives you data that can be easily used in an expression that returns a boolean.  Each user has a clearance level and a list of keys.  Each page has a required clearance level.  Each HTML element can optionally have a 'lock.'  The two expressions used are as follows:

- is a users clearance level greater than this pages clearance level?
 - every page has a clearance level requirement
 - every user has a clearance level
 - if a user has the level of the page or greater, they can view the page
 - if a user does not have the level, they are redirected to a different page
- does a user have the key that unlocks this HTML element?
 - every user has an array with a list of strings, these strings are 'keys'
 - an html element can have a 'locked' directive.
 - the locked directive requires a string.  it checks a users list of keys to see if it includes that specific string
 - if the user's key list contains that string, then a boolean is checked true.
 - the directive is a placeholder for an ngIf statement that is based on that booleans value.  

The clearance level expression is used inside of route guards to define if a user can access a certain route.
The key / lock expression is a directive that uses an ngIf statement within the HTML element itself.  If the user's permissions contains X key, that element is rendered.

### Database
The database is NoSQL using AngularFireStore.  The collection is named 'permissions' and each document id is equal to the uid of the user associated with those permissions.  

### Components
- permissions form:  simple form that allows you to change a users clearance, or add / remove a key

### Containers
There are no full routes for the permissions system

### Model
- clearance:  a number
- keys:  an array of strings

### Actions
- Create a New Users Permissions
- Grab a current users permissions from the database and add it to the app state
- Change a users permission level

### Reducers
- sets permissions in app state
- delete permissions from app state

### Effects
**Note:**  The event triggers are auth actions, not permissions actions.  
- create a new document in permissions collection with an id of the current users id
 - trigger:  auth action - createNewUser
 - input:  uid
 - logic:  create a new document using the uid as the doc id
 - dispatch:  none
- Grab a users permissions from the database and set it in the app state
 - trigger:  auth action - logIn user
 - input: uid
 - logic:  grab the users permissions from the permissions collection
 - dispatch:  set permissions in app state