export * from './auth';
export * from './profile';

import * as Auth from './auth';
import * as Profile from './profile';

export const components = [
    Auth.ForgotComponent,
    Auth.LoginComponent,
    Auth.RegisterComponent,
    Profile.ReadProfileComponent,
    Profile.UpdateProfileComponent
];
