import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

/* RBC Forms */
import * as Field from '@user/models/forms';
import { FieldConfig } from '@rbcForms/models';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {
  @Input() emailSent;

  @Output() forgot = new EventEmitter<string>();

  config: FieldConfig[] = [
    Field.emailField,
  ];

  form: any;
  valid = false;
  constructor() { }

  ngOnInit() {
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  sendResetEmail() {
    this.forgot.emit(this.form.value);
  }

}
