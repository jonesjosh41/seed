import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

/* RBC Forms */
import * as Field from '@user/models/forms';
import { FieldConfig } from '@rbcForms/models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Input() permissions;

  @Output() register = new EventEmitter<any>();

  formConfig: FieldConfig[];
  adminFormConfig: FieldConfig[];

  form: any;
  adminForm; any;
  valid = false;

  constructor() { }

  ngOnInit() {

    this.formConfig = [
      Field.emailField,
      Field.passwordField,
      Field.confirmPasswordField,
    ];

    this.adminFormConfig = [
      Field.clearanceField,
      Field.keysField,
      Field.roleField
    ];
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  registerUser() {
    const authData = {
      ...this.form.value,
      // ...this.adminForm.value,
      role: 'customer'
    };
    this.register.emit(authData);
  }

}
