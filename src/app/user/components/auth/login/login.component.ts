import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from '@angular/core';

/* AngularFire */
import { AngularFireAuth } from '@angular/fire/auth';

/* RBC Forms */
import { FieldConfig } from '@rbcForms/models';
import * as Field from '@user/models/forms';

/* My Imports */
import * as Model from '@user/models';
import * as CoreModel from '@core/models';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  @Output() emailLogin = new EventEmitter<Model.EmailLoginData>();

  config: FieldConfig[];
  form: any;
  loading: boolean;
  valid: boolean;

  constructor() { }

  ngOnInit() {
    this.loading = false;

    this.config = [
      Field.emailField,
      Field.passwordField,
    ];
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  loginUser() {
    this.emailLogin.emit(this.form.value);
  }

}
