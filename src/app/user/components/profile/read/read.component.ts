import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-read-profile',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadProfileComponent implements OnInit {
  @Input() profile;
  @Input() permissions;

  constructor() { }

  ngOnInit() {
  }

}
