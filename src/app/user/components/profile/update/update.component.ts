import {
  Component,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';

// Model Imports
import * as Model from '@user/models';
import * as CoreModel from '@core/models';
import * as UserModel from '@user/models';

// RBC Forms
import * as Field from '@user/models/forms';
import { FieldConfig } from '@rbcForms/models';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update.component.html',
  styles: [``],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateProfileComponent implements OnInit, OnChanges {
  @Input() permissions: UserModel.Permissions;
  @Input() profile: Model.ProfileData;

  @Output() update = new EventEmitter<Model.ProfileData>();

  // Support RBC Form
  formConfig: FieldConfig[];
  adminConfig: FieldConfig[];
  form: any;
  adminForm: any;
  valid = false;

  // Initialize Form Fields
  usernameField = Field.usernameField;
  emailField = Field.emailField;
  firstNameField = Field.firstNameField;
  lastNameField = Field.lastNameField;
  passwordField = Field.passwordField;

  constructor(
    private db:  AngularFirestore,
  ) {}

  ngOnChanges() {
    if (this.profile) {
    this.usernameField.value = this.profile.username;
    this.emailField.value = this.profile.email;
    this.firstNameField.value = this.profile.firstName;
    this.lastNameField.value = this.profile.lastName;
    }
  }

  ngOnInit() {
    this.formConfig = [
      this.usernameField,
      this.emailField,
      this.firstNameField,
      this.lastNameField
    ];
  }

  formChanged(value) {
    this.form = value;
    this.valid = this.form.valid;
  }

  adminFormChanged(value) {
    this.adminForm = value;
    this.valid = this.form.valid;
  }

  updateProfile() {
    const event: Model.ProfileData = {
      ...this.profile,
      ...this.form.value
    };
    this.update.emit(event);
  }
}
