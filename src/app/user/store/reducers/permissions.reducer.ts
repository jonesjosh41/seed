
import * as Action from '../actions';
import * as Model from '@user/models';

const initialState: Model.Permissions = {
    role: 'visitor',
    clearance: 0,
    keys: ['visitor'],
};

export function permissionsReducer(state = initialState, action: Action.PermissionsActions): Model.Permissions {
  switch (action.type) {

    case Action.SET_USER_PERMISSIONS: {
      return {
        ...state,
        clearance: action.payload.clearance,
        keys: action.payload.keys,
        role: action.payload.role,
      };
    }

    case Action.SET_NEW_USER_PERMISSIONS: {
      return {
        ...state,
        clearance: 1,
        keys: ['customer']
      };
    }

    case Action.CHANGE_USER_CLEARANCE_SUCCESS: {
      return {
        ...state,
        clearance: action.payload,
      };
    }

    case Action.ADD_USER_KEY_SUCCESS: {
        return {
          ...state,
          keys: action.payload,
        };
    }

    case Action.DELETE_USER_KEY_SUCCESS: {
        return {
            ...state,
            keys: action.payload
        };
    }

    case Action.CLEAR_USER_PERMISSIONS: {
      return {
        ...state,
        clearance: 0,
        keys: ['visitor']
      };
    }
  }

  return state;
}
