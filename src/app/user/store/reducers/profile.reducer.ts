import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as Model from '@user/models';

import * as Action from '../actions/profile.actions';
import { ProfileActions, ProfileActionTypes } from '../actions/profile.actions';

export interface ProfileState extends EntityState<Model.ProfileEntity> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Model.ProfileEntity> = createEntityAdapter<Model.ProfileEntity>();

const initialState: ProfileState = adapter.getInitialState({
  // additional entity state properties
});

export function profileReducer(
  state = initialState,
  action: ProfileActions
): ProfileState {
  switch (action.type) {

    case Action.ProfileActionTypes.AddProfileEntity: {
      return adapter.addOne(action.payload.profile, state);
    }

    case ProfileActionTypes.UpsertProfileEntity: {
      return adapter.upsertOne(action.payload.profile, state);
    }

    case ProfileActionTypes.AddProfileEntities: {
      return adapter.addMany(action.payload.profiles, state);
    }

    case ProfileActionTypes.UpsertProfileEntities: {
      return adapter.upsertMany(action.payload.profiles, state);
    }

    case ProfileActionTypes.UpdateProfileEntity: {
      return adapter.updateOne(action.payload.profile, state);
    }

    case ProfileActionTypes.UpdateProfileEntities: {
      return adapter.updateMany(action.payload.profiles, state);
    }

    case ProfileActionTypes.DeleteProfileEntity: {
      return adapter.removeOne(action.payload.id, state);
    }

    case ProfileActionTypes.DeleteProfileEntities: {
      return adapter.removeMany(action.payload.ids, state);
    }

    case ProfileActionTypes.ClearProfileEntities: {
      return adapter.removeAll(state);
    }

    default: {
      return state;
    }
  }
}
