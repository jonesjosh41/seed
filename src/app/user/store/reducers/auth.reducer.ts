
import * as Action from '../actions/auth.actions';
import * as Model from '@user/models';

const initialState: Model.AuthState = {
  uid: null,
  firebaseUser: null,
};

export function authReducer(state = initialState, action: Action.AuthActions): Model.AuthState {
  switch (action.type) {

    case Action.SET_USER_ID: {
      return {
        ...state,
        uid: action.payload
      };
    }

    case Action.SET_FIREBASE_USER: {
      return {
        ...state,
        firebaseUser: action.payload
      };
    }

    case Action.LOGOUT_USER: {
      return {
        ...state,
        uid: null
      };
    }
  }

  return state;
}
