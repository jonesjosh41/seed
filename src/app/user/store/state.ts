import * as UserEffect from '@user/store/effects';

import * as UserReducer from '@user/store/reducers';

import * as UserModel from '@user/models';

import { createFeatureSelector, ActionReducerMap } from '@ngrx/store';

// Model
export interface UserState {
    profile: UserReducer.ProfileState;
}

// Reducer
export const reducers: ActionReducerMap<UserState> = {
    profile: UserReducer.profileReducer
};

// Effects
export const effects: any[] = [
    UserEffect.ProfileEffects
];

// Selector
export const getUserState = createFeatureSelector<UserState>('user');
