import { Action } from '@ngrx/store';

import * as Model from '@user/models';

export const CREATE_NEW_USER_PERMISSIONS = '[Permissions Effect] Create a new users permissions in the database';
export const GET_USER_PERMISSIONS = '[Permissions Effect] Get the a users Permissions from db';
export const GET_SPECIFIC_USERS_PERMISSIONS = '[Permissions Effect] Get a specific users permissions';

export const CHANGE_USER_CLEARANCE = '[Permissions Effect] change a users clearance';
export const SET_USER_CLEARANCE = '[Permissions Reducer] set a user clearance';
export const CHANGE_USER_CLEARANCE_SUCCESS = '[Permissions] set user clearance in reducer';

export const ADD_USER_KEY = '[Permissions Effect] add a key to a users list';
export const DELETE_USER_KEY = '[Permissions Effect] delete a key from a users list';
export const ADD_USER_KEY_SUCCESS = '[Permissions Reducer] update state with added key';
export const DELETE_USER_KEY_SUCCESS = '[Permissions Reducer] update state with removed key';

export const CLEAR_USER_PERMISSIONS = '[Permissions] clear user permissions';
export const SET_USER_PERMISSIONS = '[Permissions Reducer] Set user permissions in app state';
export const SET_NEW_USER_PERMISSIONS = '[Permissions Reducer] Set permissions for newly created User';

// general permissions
export class GetUserPermissions implements Action {
    readonly type = GET_USER_PERMISSIONS;
    constructor(public payload: string) {}
}
export class GetSpecificUsersPermissions implements Action {
    readonly type = GET_SPECIFIC_USERS_PERMISSIONS;
    constructor(public payload: Model.Permissions) {}
}
export class SetUserPermissions implements Action {
    readonly type = SET_USER_PERMISSIONS;
    constructor(public payload: Model.Permissions) {}
}
export class SetNewUserPermissions implements Action {
    readonly type = SET_NEW_USER_PERMISSIONS;
}
export class CreateNewUserPermissions implements Action {
    readonly type = CREATE_NEW_USER_PERMISSIONS;
    constructor(public payload: string) {}
}

// clearance
export class ClearUserPermissions implements Action {
    readonly type = CLEAR_USER_PERMISSIONS;
}
export class ChangeUserClearance implements Action {
    readonly type = CHANGE_USER_CLEARANCE;
    constructor(public payload: number) {}
}
export class ChangeUserClearanceSuccess implements Action {
    readonly type = CHANGE_USER_CLEARANCE_SUCCESS;
    constructor(public payload: number) {}
}

// keys
export class AddUserKey implements Action {
    readonly type = ADD_USER_KEY;
    constructor(public payload: Model.ModifyKey) {}
}
export class DeleteUserKey implements Action {
    readonly type = DELETE_USER_KEY;
    constructor(public payload: Model.ModifyKey) {}
}
export class AddUserKeySuccess implements Action {
    readonly type = ADD_USER_KEY_SUCCESS;
    constructor(public payload: string[]) {}
}
export class DeleteUserKeySuccess implements Action {
    readonly type = DELETE_USER_KEY_SUCCESS;
    constructor(public payload: string[]) {}
}

export type PermissionsActions = GetUserPermissions
| GetSpecificUsersPermissions
| SetUserPermissions
| SetNewUserPermissions
| ClearUserPermissions
| ChangeUserClearance
| ChangeUserClearanceSuccess
| AddUserKey
| DeleteUserKey
| AddUserKeySuccess
| DeleteUserKeySuccess;
