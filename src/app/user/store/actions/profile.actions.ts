import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import * as Model from '@user/models';
import { ProfileEntity } from '@user/models';

// Actions that Query/Update the Database via Effect
export const GET_PROFILE = '[Profile Effect] | DB Get | Get Profile';
export const GET_PROFILES = '[Profile Effect] | DB Get | Get Set of Profiles';
export const GET_EMPLOYEE_PROFILES = '[Profile Effect] | DB Get | Get Employee Profiles';
export const GET_CUSTOMER_PROFILES = '[Profile Effect] | DB Get | Get Customer Profiles';
export const CREATE_PROFILE = '[Profile Effect] | DB Set | Create Profile';
export const UPDATE_PROFILE = '[Profile Effect] | DB Update | Update Profile';
export const DELETE_PROFILE = '[Profile Effect] | DB Remove | Delete Profile';


export class GetProfile implements Action {
  readonly type = GET_PROFILE;
  // constructor(public payload: Model.ProfileEffectPayload<string>) {}
}

export class GetProfiles implements Action {
  readonly type = GET_PROFILES;
  constructor(public payload: string) {}
}

export class CreateProfile implements Action {
  readonly type = CREATE_PROFILE;
  constructor(public payload: any) {}
}

export class UpdateProfile implements Action {
  readonly type = UPDATE_PROFILE;
  constructor(public payload: Model.ProfileData) {}
}

export class DeleteProfile implements Action {
  readonly type = DELETE_PROFILE;
  constructor(public payload: Model.ProfileData) {}
}


// Actions That Update The State via Reducer
export enum ProfileActionTypes {
  AddProfileEntity = '[Profile Reducer] | Entity | Add Profile',
  UpsertProfileEntity = '[Profile Reducer] | Entity | Upsert Profile',
  AddProfileEntities = '[Profile Reducer] | Entity | Add Profiles',
  UpsertProfileEntities = '[Profile Reducer] | Entity | Upsert Profiles',
  UpdateProfileEntity = '[Profile Reducer] | Entity | Update Profile',
  UpdateProfileEntities = '[Profile Reducer] | Entity | Update Profiles',
  DeleteProfileEntity = '[Profile Reducer] | Entity | Delete Profile',
  DeleteProfileEntities = '[Profile Reducer] | Entity | Delete Profiles',
  ClearProfileEntities = '[Profile Reducer] | Entity | Clear Profiles'
}

export class AddProfileEntity implements Action {
  readonly type = ProfileActionTypes.AddProfileEntity;
  constructor(public payload: { profile: ProfileEntity }) {}
}

export class UpdateProfileEntity implements Action {
  readonly type = ProfileActionTypes.UpdateProfileEntity;
  constructor(public payload: { profile: Update<ProfileEntity> }) {}
}

export class UpsertProfileEntity implements Action {
  readonly type = ProfileActionTypes.UpsertProfileEntity;
  constructor(public payload: { profile: ProfileEntity }) {}
}

export class AddProfileEntities implements Action {
  readonly type = ProfileActionTypes.AddProfileEntities;
  constructor(public payload: { profiles: ProfileEntity[] }) {}
}

export class UpsertProfileEntities implements Action {
  readonly type = ProfileActionTypes.UpsertProfileEntities;
  constructor(public payload: { profiles: ProfileEntity[] }) {}
}

export class UpdateProfileEntities implements Action {
  readonly type = ProfileActionTypes.UpdateProfileEntities;
  constructor(public payload: { profiles: Update<ProfileEntity>[] }) {}
}

export class DeleteProfileEntity implements Action {
  readonly type = ProfileActionTypes.DeleteProfileEntity;
  constructor(public payload: { id: string }) {}
}

export class DeleteProfileEntities implements Action {
  readonly type = ProfileActionTypes.DeleteProfileEntities;
  constructor(public payload: { ids: string[] }) {}
}

export class ClearProfileEntities implements Action {
  readonly type = ProfileActionTypes.ClearProfileEntities;
}

export type ProfileActions = AddProfileEntity
 | UpdateProfileEntity
 | UpsertProfileEntity
 | DeleteProfileEntity
 | AddProfileEntities
 | UpsertProfileEntities
 | UpdateProfileEntities
 | DeleteProfileEntities
 | ClearProfileEntities
 | CreateProfile
 | GetProfile
 | GetProfiles
 | DeleteProfile
 | UpdateProfile;
