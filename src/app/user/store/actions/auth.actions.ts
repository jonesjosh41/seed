import { Action } from '@ngrx/store';

export const LOGOUT_USER = '[Auth] Delete Token and User Attributes';

export const SET_FIREBASE_USER = '[Auth] Set Firebase User';
export const SET_USER_ID = '[Auth] Set User ID';

export class SetUserID implements Action {
  readonly type = SET_USER_ID;
  constructor(public payload: string) {}
}
export class SetFirebaseUser implements Action {
  readonly type = SET_FIREBASE_USER;
  constructor(public payload: any) {}
}
export class LogoutUser implements Action { readonly type = LOGOUT_USER; }

export type AuthActions =
| LogoutUser
| SetUserID
| SetFirebaseUser;
