import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { AngularFireAuth } from '@angular/fire/auth';

/* RXJS */
import { switchMap, catchError, map, pluck, tap, concatMap, debounceTime, delay } from 'rxjs/operators';
import { from, of } from 'rxjs';

/* App Imports */
import * as coreAction from '@core/store/actions';
import * as Action from '@user/store/actions';
import * as Model from '@user/models';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private af: AngularFireAuth
  ) {}

  @Effect()
  logoutUser$ = this.actions$
    .ofType(Action.LOGOUT_USER)
    .pipe(
      tap(() => this.af.auth.signOut()),
      map(() => new coreAction.Go({ path: ['/']})),
      catchError((err) => of(new coreAction.ShowSnackbar(err.message)))
    );
}
