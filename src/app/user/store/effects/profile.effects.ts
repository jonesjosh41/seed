import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

/* NGRX */
import { Store, select } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

/* RXJS */
import { switchMap, catchError, map, pluck, tap, concatMap, withLatestFrom } from 'rxjs/operators';
import { from, of, forkJoin } from 'rxjs';

/* Store */
import * as CoreAction from '@core/store/actions';
import * as Action from '../actions';
import * as Model from '../../models';
import { getUidAndRole } from '@user/store/selectors/permissions.selector';
import { ProfileState } from '../reducers';
import { ProfileData } from '../../models';

@Injectable()
export class ProfileEffects {
  constructor(
    private actions$: Actions,
    private db: AngularFirestore,
    private store: Store<any>,
  ) {}

  @Effect()
  getSingle$ = this.actions$
    .ofType(Action.GET_PROFILE)
    .pipe(
        withLatestFrom(this.store.pipe(select(getUidAndRole))),
        switchMap(([action, auth]) => this.db
            .collection('users')
            .doc(auth.role)
            .collection('profiles')
            .doc<Model.ProfileData>(auth.uid)
            .valueChanges()
        ),
        map((profile) => new Action.AddProfileEntity({ profile: { id: profile.id, data: { ...profile }}})),
        catchError(err => of(console.log(err)))
    );

  @Effect({ dispatch: false })
  create$ = this.actions$
      .ofType(Action.CREATE_PROFILE)
      .pipe(
        map((action: Action.CreateProfile): ProfileData => action.payload),
        map(payload => {
          return {
            id: payload.id,
            email: payload.email,
            role: payload.role,
            username: '',
            firstName: '',
            lastName: '',
          };
        }),
        // Add a profile within the sub collection of the role given in payload
        tap((payload) => {
          // set role as 'customer' if no role is given.  i.e. 'customer' is default role
          this.db
            .collection('users')
            .doc(payload.role)
            .collection('profiles')
            .doc(payload.id)
            .set(payload);
        }),
      );

  @Effect()
  getList$ = this.actions$
    .ofType(Action.GET_PROFILES)
    .pipe(
        pluck('payload'),
        // Get a set of profiles with the role given in the payload
        switchMap((role: string) => this.db
            .collection('users')
            .doc(role)
            .collection<Model.ProfileData>('profiles')
            .valueChanges()
        ),
        // Transform the set of profiles into a set of Entities
        map((profileDataArray: Model.ProfileData[]) => profileDataArray.map((profile) => {
          return { id: profile.id, data: { ...profile } };
        })),
        // Add set of Entities to the profile state
        map((profileEntityArray: Model.ProfileEntity[]) => new Action.AddProfileEntities( { profiles: profileEntityArray })),
        catchError(err => of(console.log(err)))
    );

  @Effect()
  update$ = this.actions$
    .ofType(Action.UPDATE_PROFILE)
    .pipe(
      pluck('payload'),
      // Update the Firestore Database
      tap((payload: Model.ProfileData) => this.db
        .collection('users')
        .doc(payload.role)
        .collection('profiles')
        .doc(payload.id)
        .update(payload)
      ),
      // Grab Data from Firestore Database
      switchMap((payload: Model.ProfileData) => this.db
        .collection('users')
        .doc(payload.role)
        .collection('profiles')
        .doc(payload.id)
        .valueChanges()
      ),
      concatMap((profile: Model.ProfileData) => [
        // Set the Application state in the reducer, payload is in an Entity format
        new Action.UpsertProfileEntity({ profile: { id: profile.id, data: { ...profile }}}),
        // Give the User Feedback assuming the db set was successful
        new CoreAction.ShowSnackbar('Your Profile Has Been Updated')
      ]),
      catchError((err) => forkJoin(() => [
        // Log the Error
        console.log(err),
        // Give user feedback on the error
        new CoreAction.ShowSnackbar(err.message)
      ]))
    );

  @Effect()
  delete$ = this.actions$
    .ofType( Action.ProfileActionTypes.DeleteProfileEntity)
    .pipe(
        pluck('payload'),
        tap((payload: Model.ProfileEffectPayload<string>) => this.db
            .collection('users')
            .doc(payload.role)
            .collection('profiles')
            .doc<Model.ProfileData>(payload.data)
            .delete()
        ),
        map(payload => new Action.DeleteProfileEntity({ id: payload.data }))
    );
}
