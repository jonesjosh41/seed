import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';

/* RXJS */
import { switchMap, catchError, map, concatMap, tap, pluck } from 'rxjs/operators';
import { from, of } from 'rxjs';

/* Store */
import * as Action from '../actions';
import * as CoreAction from '@core/store/actions';
import * as Model from '@user/models';

/* Angular Fire */
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable()
export class PermissionsEffects {

    documentId: string;

  constructor(
    private actions$: Actions,
    private afAuth: AngularFireAuth,
    private db: AngularFirestore
  ) { }

    @Effect({ dispatch: false })
    changeUserClearance$ = this.actions$
        .ofType(Action.CHANGE_USER_CLEARANCE)
        .pipe(
            pluck('payload'),
            tap((clearance: number) => from(
                this.db.doc<Model.Permissions>(this.documentId)
                .update({ clearance: clearance }))
                .pipe(  catchError(err => of(console.log(err)))  )
            ),
            map((clearance: number) => new Action.ChangeUserClearanceSuccess(clearance)),
            catchError((err) => of(console.log(err)))
        );

    @Effect()
    addUserKey$ = this.actions$
        .ofType(Action.ADD_USER_KEY)
        .pipe(
            pluck('payload'),
            map((payload: Model.ModifyKey) => {
                const keys = [...payload.current, payload.new];
                const uid = payload.uid;
                return { keys: keys, uid: uid };
            }),
            tap((data) => from(this.db
                .doc<Model.Permissions>(this.documentId)
                .update({ keys: data.keys}))
                .pipe(  catchError((err) => of(console.log(err)))  )
            ),
            pluck('keys'),
            map((keys: string[]) => new Action.AddUserKeySuccess(keys)),
            catchError((err) => of(console.log(err)))
        );

    @Effect()
    deleteUserKey$ = this.actions$
        .ofType(Action.DELETE_USER_KEY)
        .pipe(
            pluck('payload'),
            map((payload: Model.ModifyKey) => {
                const keys = [...payload.current].filter((key) => key !== payload.new );
                const uid = payload.uid;
                return { keys: keys, uid: uid };
            }),
            tap((data) => from(this.db
                .doc<Model.Permissions>('permissions/${data.uid}')
                .update({ keys: data.keys }))
                .pipe(  catchError(err => of(console.log(err))))
            ),
            pluck('keys'),
            map((keys: string[]) => new Action.DeleteUserKeySuccess(keys)),
            catchError((err) => of(console.log(err)))
        );

    @Effect()
    setNewUserPermissions$ = this.actions$
        .ofType(Action.CREATE_NEW_USER_PERMISSIONS)
        .pipe(
            pluck('payload'),
            switchMap((uid: string) => this.db
                .collection('permissions')
                .doc<Model.Permissions>(uid)
                .set({ clearance: 1, keys: ['customer'], role: 'customer'})
            ),
            map(() => new Action.SetNewUserPermissions())
        );

    @Effect()
    getPermissions$ = this.actions$
        .ofType(Action.GET_USER_PERMISSIONS)
        .pipe(
            pluck('payload'),
            switchMap((uid: string) => this.db
                .collection('permissions')
                .doc<Model.Permissions>(uid)
                .valueChanges()
            ),
            map((document: Model.Permissions) => new Action.SetUserPermissions(document)),
            catchError((err) => of(console.log(err)))
        );

    @Effect()
    clearUserPermissions$ = this.actions$
        .ofType(Action.LOGOUT_USER)
        .pipe(
            map(() => new Action.ClearUserPermissions)
        );

}
