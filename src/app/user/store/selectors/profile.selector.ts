/*
DATA TO GET:
1:  Array of Users
2:  Specific User By uid
*/

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as ProfileReducer from '@user/store/reducers/profile.reducer';
import * as Model from '@user/models';

import { getCurrentUid } from '@user/store/selectors/auth.selector';
import { getUserState } from '@user/store/state';
import { getRouterParamId } from '@core/store/selectors';

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = ProfileReducer.adapter.getSelectors();

export const getProfileState = createSelector(
  getUserState,
  (state) => state.profile
);

export const getArrayOfProfiles = createSelector(
  getProfileState,
  (eventState): Model.ProfileData[] => {
    const entities = eventState.entities;
    return Object.keys(entities)
      .map(id => entities[id])
      .map(event => event.data);
  }
);

export const getProfileEntities = createSelector(
  getProfileState,
  (state) =>  state.entities
);

export const getCurrentUserProfile = createSelector(
  getCurrentUid,
  getProfileEntities,
  (uid: string, entities) => {
    return entities[uid] ? entities[uid].data : null;
  }
);

export const getProfileFromRouteParams = createSelector(
  getRouterParamId,
  getProfileEntities,
  (id: number, entities): Model.ProfileData => entities[id] ? entities[id].data : null
);

