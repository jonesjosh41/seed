/*
DATA TO GET:
1:  User Id
 */

 import { createSelector, createFeatureSelector } from '@ngrx/store';

 export const getAuthState = createFeatureSelector<Model.AuthState>('auth');

 import * as Model from '@user/models';

 export const getCurrentUid = createSelector(
     getAuthState,
     (state: Model.AuthState) => state.uid
 );

 export const getFirebaseUser = createSelector(
     getAuthState,
     (state: Model.AuthState) => state.firebaseUser
 );


