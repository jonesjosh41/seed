/*
Data To Get:
1:  User Clearance
2:  User Keys
*/

import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as Model from '@user/models';
import { getPageClearance, getPageKeys } from '@core/store/selectors';
import { getCurrentUid } from '@user/store/selectors/auth.selector';

export const getPermissionsState = createFeatureSelector<Model.Permissions>('permissions');

export const getRole = createSelector(
    getPermissionsState,
    (state): string => state.role
);

export const getClearance = createSelector(
    getPermissionsState,
    (state): number => state.clearance
);

export const getKeys = createSelector(
    getPermissionsState,
    (state): string[] => state.keys
);

export const hasPageClearance = createSelector(
    getClearance,
    getPageClearance,
    (clearance: number, pageClearance: number): boolean => clearance >= pageClearance ? true : false
);

export const hasPageKey = createSelector(
    getKeys,
    getPageKeys,
    (userKeys, pageKeys): boolean => {
        userKeys.forEach(userKey => {
            if (pageKeys.includes(userKey)) { return true; }
        });
        return false;
    }
);

export const hasPagePermission = createSelector(
    hasPageClearance,
    hasPageKey,
    (hasClearance, hasKey): boolean => hasClearance && hasKey ? true : false
);

export const getProfileEffectPayloadData = createSelector(
    getCurrentUid,
    getRole,
    (id: string, role: string) => ({ id: id, role: role })
);

export const getUidAndRole = createSelector(
    getCurrentUid,
    getRole,
    (uid: string, role: string) => {
       return { role: role, uid: uid };
    }
);



