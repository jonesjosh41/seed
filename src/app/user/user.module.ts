import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Routes
import { ROUTES } from './containers/routes';

// Components
import * as Component from './components';

// Containers
import * as Container from './containers';

// Store
import { reducers, effects } from './store/state';

// Form
import { RBCFormsModule } from '../forms/forms.module';
import { formFieldComponents } from '../forms/components';

@NgModule({
  declarations: [
    ...Container.containers,
    ...Component.components,
  ],
  imports: [
    RBCFormsModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    StoreModule.forFeature('user', reducers),
    EffectsModule.forFeature(effects),
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  entryComponents: [
    ...formFieldComponents
  ],
  providers: [
  ]
})
export class UserModule { }
