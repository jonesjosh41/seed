import { Component, OnInit } from '@angular/core';

/* NGRX & RXJS */
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

/* RBC Forms */
import { FieldConfig } from '@rbcForms/models';
import * as Field from '@user/models/forms';

/* My Imports */
import * as Model from '@user/models';
import * as Action from '@user/store/actions';
import * as Selector from '@user/store/selectors';
import * as CoreAction from '@core/store/actions';
import * as CoreSelector from '@core/store/selectors';
import * as CoreStore from '@core/store';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  view$: Observable<string>;
  permissions$: Observable<Model.Permissions>;
  emailSent: boolean;

  constructor(
    private store: Store<any>,
    private af: AngularFireAuth
  ) { }

  ngOnInit() {
    this.permissions$ = this.store.select(Selector.getPermissionsState);
    this.view$ = this.store.select(CoreSelector.getPageView);
  }

  register(authData) {
    this.af.auth.createUserWithEmailAndPassword(authData.email, authData.password)
      .then((user) => {
        const uid = user.user.uid;
        this.store.dispatch(new Action.SetUserID(uid)); // Set user id in state
        this.store.dispatch(new Action.CreateNewUserPermissions(uid)); // create permissions set in database
        this.store.dispatch(new Action.SetNewUserPermissions()); // Set base permissions for this user in state
        this.store.dispatch(new Action.CreateProfile({
            id: uid,
            email: user.user.email,
            role: authData.role,
        }));

        this.store.dispatch(new CoreStore.Go({ path: [`/user/profile`]}));
      })
      .catch((err) => this.store.dispatch(new CoreStore.ShowSnackbar(err.message)));
  }

  resetForgottenPassword(authData) {
    this.af.auth.sendPasswordResetEmail(authData.email);
    this.store.dispatch(new CoreStore.ShowSnackbar('An email has been sent with a password reset link'));
    this.emailSent = true;
  }

  emailLogin(authData) {
    this.af.auth.signInWithEmailAndPassword(authData.email, authData.password)
      .then((user) => {
        const uid = user.user.uid;
        this.store.dispatch(new Action.SetUserID(uid));
        this.store.dispatch(new Action.GetUserPermissions(uid));
        this.store.dispatch(new CoreStore.Go({path: [`/user/profile`]}));
      })
      .catch(err => this.store.dispatch(new CoreStore.ShowSnackbar(err.message)));
  }

  oAuthLogin(provider) {
    return this.af.auth
      .signInWithPopup(provider)
        .catch(error => new CoreAction.ShowSnackbar(error));
  }

  anonymousLogin(authData) {
    return this.af.auth
      .signInAnonymously()
        .catch(error => new CoreAction.ShowSnackbar(error));
  }

}
