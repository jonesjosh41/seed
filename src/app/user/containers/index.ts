export * from './auth/auth.component';
export * from './profile/profile.component';

import { AuthComponent } from './auth/auth.component';
import { ProfileComponent } from './profile/profile.component';


export const containers = [
    AuthComponent,
    ProfileComponent
];
