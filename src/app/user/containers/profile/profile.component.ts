import { Component, OnInit } from '@angular/core';

/* NGRX & RXJS */
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators';

/* My Imports */
import * as Model from '@user/models';
import * as Action from '@user/store/actions';
import * as Selector from '@user/store/selectors';
import * as CoreSelector from '@core/store/selectors';
import * as CoreStore from '@core/store';
import { viewTypeField } from '@user/models/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  view$: Observable<string>;
  currentProfile$: Observable<Model.ProfileData>;
  selectedProfile$: Observable<Model.ProfileData>;
  permissions$: Observable<Model.Permissions>;

  // Local Header View Settings
  currentView: 'Profile' | 'Account History' | 'Payment Options' | 'Membership' | 'Connections' | 'Saved Addresses';
  viewSelectForm = [ viewTypeField ];

  constructor(
    private store: Store<any>
  ) { }

  ngOnInit() {
    this.view$ = this.store.select(CoreSelector.getPageView);
    this.currentProfile$ = this.store.select(Selector.getCurrentUserProfile);
    this.selectedProfile$ = this.store.select(Selector.getProfileFromRouteParams);
    this.permissions$ = this.store.select(Selector.getPermissionsState);

    this.currentView = 'Profile';

    // Get the Users Profile After Permissions Are Available In State
    this.permissions$
      .pipe(delay(1000))
      .subscribe(() => this.store.dispatch(new Action.GetProfile()));
  }

  changeView(value) {
    this.currentView = value.get('currentView').value;
  }

  updateCurrentProfile(profile: Model.ProfileData) {
    this.store.dispatch(new Action.UpdateProfile(profile));
  }

  updateSelectedProfile(profile: Model.ProfileData) {
    this.store.dispatch(new Action.UpdateProfile(profile));
  }

}
