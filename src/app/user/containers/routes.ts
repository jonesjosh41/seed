import { Routes } from '@angular/router';
import * as Container from './index';

export const ROUTES: Routes = [
    // Auth Containers
    {
        path: 'login',
        component: Container.AuthComponent,
        data: {
            view: 'LOGIN'
        }
    },
    {
        path: 'register',
        component: Container.AuthComponent,
        data: {
            view: 'REGISTER'
        }
    },
    {
        path: 'forgot',
        component: Container.AuthComponent,
        data: {
            view: 'FORGOT'
        }
    },
    // Profile Containers
    {
        path: 'profile/:id/update',
        component: Container.ProfileComponent,
        data: {
            view: 'UPDATE SELECTED'
        }
    },
    {
        path: 'profile',
        component: Container.ProfileComponent,
        data: {
            view: 'UPDATE CURRENT'
        }
    },
    {
        path: 'profile/:id',
        component: Container.ProfileComponent,
        data: {
            view: 'READ SELECTED'
        }
    },
];
