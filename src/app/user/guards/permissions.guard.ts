import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { hasPagePermission } from '@user/store/selectors';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  userPermission$: Observable<boolean> = this.store.select(hasPagePermission);

  constructor(
    private store: Store<any>
  ) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.userPermission$.pipe(
      map((hasPermission) => {
        if (hasPermission === true) {
          return true;
        } else {
          return false;
        }
      })
    );
  }
}
