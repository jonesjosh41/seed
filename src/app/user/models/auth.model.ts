import { firebase } from '@firebase/app';

export interface AuthState {
    uid: string;
    firebaseUser: FirebaseUser;
}

export interface FirebaseUser {
    displayName?: any;
    email: any;
    emailVerified: boolean;
    isAnonymous: boolean;
    metadata: firebase.auth.UserMetadata;
    phoneNumber: string | null;
    photoURL: any;
    providerData: firebase.UserInfo[];
    providerId: any;
    uid: firebase.User;
}

export interface EmailLoginData {
    email: string;
    password: string;
}
