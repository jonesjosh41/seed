export interface ProfileEntity {
    id: string;
    data: ProfileData;
}

export interface ProfileData {
    id: string;
    role: string;
    email: string;
    username: string;
    firstName: string;
    lastName: string;
    photoURL?: string;
}

export interface ProfileEffectPayload<T> {
    role?: string;
    data: T;
}
