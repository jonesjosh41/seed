export interface Permissions {
    role: string;
    clearance:  number;
    keys: string[];
}

export interface ModifyKey {
    new: string;
    current: string[];
    uid: string;
}
