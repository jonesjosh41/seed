import { FieldConfig } from '@rbcForms/models';

export const usernameField: FieldConfig = {
    value: '',
    formControlName: 'username',
    componentType: 'input',
    label: 'Username',
    extras: {
        help: 'type your email here',
    },
    inputConfig: {
        type: 'text',
        required: true,
    },
};

