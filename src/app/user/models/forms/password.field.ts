import { FieldConfig } from '../../../forms/models';
import { Validators } from '@angular/forms';
import * as custValidator from './validators';

export const passwordField: FieldConfig = {
    value: '',
    formControlName: 'password',
    componentType: 'input',
    label: 'password',
    inputConfig: {
        type: 'password',
        required: true,
    },
    extras: {
        hint: 'lowercase, uppercase, number, special character required',
        help: 'type your password here',
    },
    validators: [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern('^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[a-z])(?=.*?[#?!@$%^&*-]).{8,}$'),
     ],
    errors: [
        {
            validatorProperty: 'required',
            message: 'password is required'
        },
        {
            validatorProperty: 'minlength',
            message: 'Your password must have at least 8 characters',
        },
        {
            validatorProperty: 'pattern',
            message: 'you must have a lowercase, uppercase, number, and special character'
        }
    ]
};

 export const confirmPasswordField: FieldConfig = {
    value: '',
    formControlName: 'confirmPassword',
    componentType: 'input',
    label: 'Confirm Password',
    inputConfig: {
        type: 'password',
        required: true,
    },
    validators: [Validators.required ],
    crossFieldValidators: [custValidator.passwordMatch],
    errors: [
        {
            validatorProperty: 'required',
            message: 'you must confirm your password'
        },
    ],
    crossFieldErrors: [
        {
            validatorProperty: 'passwordMatch',
            message: 'passwords do not match',
        }
    ],
    extras: {
        help: 'type the same password as above',
    },
};
