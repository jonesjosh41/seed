import { FieldConfig } from '../../../forms/models';
import { Validators } from '@angular/forms';
import * as custValidator from './validators';

export const viewTypeField: FieldConfig = {
    formControlName: 'currentView',
    componentType: 'select',
    value: 'Profile',
    label: 'Current Setting',
    selectOptions: [
        'Profile',
        'Permissions',
        'Account History',
        'Saved Addresses',
        'Payment Options',
        'Membership',
        'Connections'
    ],
    displayConfig: {
        size: '100%',
        marginTop: '10%'
    },
    extras: {
        help: 'choose the format to view events'
    }
};
