import { FieldConfig } from '../../../forms/models';

 export const clearanceField: FieldConfig = {
    value: '',
    formControlName: 'clearance',
    componentType: 'select',
    label: 'Clearance',
    displayConfig: {
        size: '30%'
    },
};

export const keysField: FieldConfig = {
    value: '',
    formControlName: 'keys',
    componentType: 'select',
    label: 'Keys',
    displayConfig: {
        size: '30%'
    },
};

export const roleField: FieldConfig = {
    value: '',
    formControlName: 'role',
    componentType: 'select',
    label: 'Role',
    displayConfig: {
        size: '30%'
    },
};
