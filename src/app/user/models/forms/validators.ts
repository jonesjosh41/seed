import { ValidatorFn, FormGroup } from '@angular/forms';

export const passwordMatch: ValidatorFn = (group: FormGroup) => {
    const password = group.get('password').value;
    const confirmPassword = group.get('confirmPassword').value;

    return password === confirmPassword
        ? null
        : { passwordMatch: true };
};


