import { FieldConfig } from '../../../forms/models';
import { Validators } from '@angular/forms';
import * as custValidator from './validators';

 export const emailField: FieldConfig = {
    value: '',
    formControlName: 'email',
    componentType: 'input',
    label: 'Email',
    extras: {
        help: 'type your email here',
    },
    inputConfig: {
        type: 'text',
        required: true,
    },
    validators: [Validators.required, Validators.email],
    errors: [
      {
        validatorProperty: 'required',
        message: 'email is required'
      },
      {
        validatorProperty: 'email',
        message: 'Invalid email format'
      }
    ]
};
