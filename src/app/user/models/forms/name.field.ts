import { FieldConfig } from '../../../forms/models';
import { Validators } from '@angular/forms';
import * as custValidator from './validators';

export const firstNameField: FieldConfig = {
    displayConfig: {
        size: '47%',
        marginRight: '4%',
        marginLeft: '0%',
    },
    value: '',
    formControlName: 'firstName',
    componentType: 'input',
    label: 'first name',
    inputConfig: {
        type: 'text',
    }
};

 export const lastNameField: FieldConfig = {
    displayConfig: {
        size: '47%',
        marginRight: '0%'
    },
    value: '',
    formControlName: 'lastName',
    componentType: 'input',
    label: 'last name',
    inputConfig: {
        type: 'text',
    }
};
